//
//  registerViewController.m
//  ScarCamo
//
//  Created by mac on 19/02/18.
//  Copyright © 2018 RohitMahajan. All rights reserved.
//

#import "registerViewController.h"
#import "AFNetworking.h"
#import "introViewController.h"
#import "MBProgressHUD.h"

@interface registerViewController ()<UITextFieldDelegate>
{
    MBProgressHUD *HUD;
    UIScrollView *scrv;

}
@end

@implementation registerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIImageView *bg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    bg.image=[UIImage imageNamed:@"login-bg"];
    [self.view addSubview:bg];
    
    
    
    scrv=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    scrv.contentSize=CGSizeMake(0, self.view.frame.size.height+110);
    [self.view addSubview:scrv];
    
    
    UIImageView *logo=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-95, 60, 190, 30)];
    logo.image=[UIImage imageNamed:@"logo"];
    logo.contentMode = UIViewContentModeScaleAspectFit;
    [scrv addSubview:logo];
    
    username=[[UITextField alloc]initWithFrame:CGRectMake(20, self.view.frame.size.height/2-70, self.view.frame.size.width-40, 40)];
    username.font = [UIFont fontWithName:@"Helvetica" size:15];
    username.textColor=[UIColor whiteColor];
    username.textAlignment=NSTextAlignmentCenter;
    username.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    username.layer.borderColor=[UIColor whiteColor].CGColor;
    username.layer.borderWidth=1.0;
    username.delegate=self;
    [scrv addSubview:username];
    
    
    UILabel *loginLbl=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-100, username.frame.origin.y-80, 200, 40)];
    loginLbl.text=@"SIGN UP";
    loginLbl.font = [UIFont fontWithName:@"Helvetica" size:22];
    loginLbl.textColor=[UIColor whiteColor];
    loginLbl.textAlignment=NSTextAlignmentCenter;
    [scrv addSubview:loginLbl];
    
    
    
    
    password=[[UITextField alloc]initWithFrame:CGRectMake(20, self.view.frame.size.height/2-20, self.view.frame.size.width-40, 40)];
    password.textAlignment=NSTextAlignmentCenter;
    password.textColor=[UIColor whiteColor];
    password.secureTextEntry=true;
    password.font = [UIFont fontWithName:@"Helvetica" size:15];
    password.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    password.layer.borderColor=[UIColor whiteColor].CGColor;
    password.layer.borderWidth=1.0;
    password.delegate=self;
    [scrv addSubview:password];
    
    email=[[UITextField alloc]initWithFrame:CGRectMake(20, self.view.frame.size.height/2+30, self.view.frame.size.width-40, 40)];
    email.textColor=[UIColor whiteColor];
    email.textAlignment=NSTextAlignmentCenter;
    email.font = [UIFont fontWithName:@"Helvetica" size:15];
    email.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    email.layer.borderColor=[UIColor whiteColor].CGColor;
    email.layer.borderWidth=1.0;
    email.delegate=self;
    email.keyboardType=UIKeyboardTypeEmailAddress;
    [scrv addSubview:email];
    
    adress=[[UITextField alloc]initWithFrame:CGRectMake(20, self.view.frame.size.height/2+80, self.view.frame.size.width-40, 40)];
    adress.textColor=[UIColor whiteColor];
    adress.textAlignment=NSTextAlignmentCenter;
    adress.font = [UIFont fontWithName:@"Helvetica" size:15];
    adress.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Address" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    adress.layer.borderColor=[UIColor whiteColor].CGColor;
    adress.layer.borderWidth=1.0;
    adress.delegate=self;
   // [self.view addSubview:adress];
    
    
    UIButton *loginButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [loginButton addTarget:self action:@selector(registerAction:) forControlEvents:UIControlEventTouchUpInside];
    [loginButton setTitle:@"SIGN UP" forState:UIControlStateNormal];
    loginButton.frame = CGRectMake(self.view.frame.size.width/2-75, adress.frame.origin.y+adress.frame.size.height+15, 150, 50);
    loginButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
    [loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    loginButton.layer.borderWidth=1.0;
    loginButton.layer.borderColor=[UIColor whiteColor].CGColor;
    [scrv addSubview:loginButton];
    
    UIButton *signUp = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [signUp addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    [signUp setTitle:@"Already have an account" forState:UIControlStateNormal];
    signUp.frame = CGRectMake(self.view.frame.size.width/2-100, self.view.frame.size.height-85, 200, 20);
    signUp.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
    [signUp setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [scrv addSubview:signUp];
    
    UIImageView *arrow_img=[[UIImageView alloc]initWithFrame:CGRectMake(loginButton.frame.size.width-30, loginButton.frame.size.height/2-10, 20, 20)];
    arrow_img.image=[UIImage imageNamed:@"arrow"];
    [loginButton addSubview:arrow_img];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    //    [scrv setContentOffset:CGPointMake(0, 0) animated:YES];
    [textField resignFirstResponder];
    return YES;
}
-(void)registerAction:(UIButton*)btn
{
    HUD=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText=@"Please Wait";
    NSUserDefaults *u = [NSUserDefaults standardUserDefaults];
    NSString *emaillll=[u objectForKey:@"deviceToken"];

    NSDictionary *params = @{@"username"       : username.text,
                             @"email"          : email.text,
                             @"password"       : password.text,
                             @"deviceType"     : @"1",
                             @"deviceToken"    : emaillll
                             };
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    [manager POST:@"http://18.219.134.84/slim_api/public/users/signup" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"success!");
        NSLog(@"JSON: %@", responseObject);
        HUD.hidden=true;
        if([[responseObject valueForKey:@"code"]intValue]==1)
        {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:[[responseObject valueForKey:@"result"]valueForKey:@"id"] forKey:@"userid"];
        [userDefaults synchronize];
            
            [self.navigationController popViewControllerAnimated:YES];
            
//            introViewController *smvc = [[introViewController alloc]init];
//            [self.navigationController pushViewController:smvc animated:YES];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                                message:[responseObject valueForKey:@"message"]
                                                               delegate:nil
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil];
            [alertView show];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
    }];
    
    
    
    

   
    
}
-(void)backAction:(UIButton*)btn
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
