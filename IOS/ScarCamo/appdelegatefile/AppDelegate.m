//
//  AppDelegate.m
//  RemiderApp
//
//  Created by mac on 19/01/18.
//  Copyright © 2018 RohitMahajan. All rights reserved.
//

#import "AppDelegate.h"
#import "introViewController.h"
#import <UserNotifications/UserNotifications.h>

#import <Stripe/Stripe.h>
#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
@interface AppDelegate ()<UNUserNotificationCenterDelegate>
{
    NSString *userid;
}
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if(!error){
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            }
        }];
    }
    else {
        // Code for old versions
    }
    

        [[STPPaymentConfiguration sharedConfiguration] setPublishableKey:@"pk_test_DwkBrpGS1ZTZd4qZd4i8eKlk"];
    [[STPPaymentConfiguration sharedConfiguration] setAppleMerchantIdentifier:@"merchant.com.apphub.stripe"];

      UINavigationController *navController = (UINavigationController *)self.window.rootViewController;
    introViewController *controller = [[introViewController alloc]init];
    navController.navigationBarHidden=YES;
    [navController setViewControllers:@[controller]];
    
    
    
    
    return YES;
}
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    NSLog(@"User Info : %@",notification.request.content.userInfo);
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
}

//Called to let your app know which action was selected by the user for a given notification.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler{
    NSLog(@"User Info : %@",response.notification.request.content.userInfo);
    completionHandler();
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    
    NSString *dt=[NSString stringWithFormat:@"%@",deviceToken];
    NSString *stringWithoutSpaces = [dt stringByReplacingOccurrencesOfString:@" " withString:@""];
    stringWithoutSpaces = [stringWithoutSpaces stringByReplacingOccurrencesOfString:@"<" withString:@""];
    stringWithoutSpaces = [stringWithoutSpaces stringByReplacingOccurrencesOfString:@">" withString:@""];
    NSLog(@"Did Register for Remote Notifications with Device Token (%@)", stringWithoutSpaces);
    
    
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:stringWithoutSpaces forKey:@"deviceToken"];
    [userDefaults synchronize];
    
    
    
    // custom stuff we do to register the device with our AWS middleman
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Did Fail to Register for Remote Notifications");
    NSLog(@"%@, %@", error, error.localizedDescription);
    
}
- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
{
//    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
//    self.notification=true;
//    self.notiNewsId=[[[userInfo valueForKey:@"aps"]valueForKey:@"alert"]valueForKey:@"news_id"];
//
//    NSLog(@"%@",[[[userInfo valueForKey:@"aps"]valueForKey:@"alert"]valueForKey:@"news_id"]);
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"userInfo.plist"];
//
//    [userInfo writeToFile:filePath atomically:YES];
//
//
//
//    if(application.applicationState == UIApplicationStateActive) {
//
//        //app is currently active, can update badges count here
//
//    }else if(application.applicationState == UIApplicationStateBackground){
//
//        [notificationCenter postNotificationName:@"notification" object:nil userInfo:nil];
//
//    }else if(application.applicationState == UIApplicationStateInactive){
//
//        [notificationCenter postNotificationName:@"notification" object:nil userInfo:nil];
//
//    }
    
    
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:@"no" forKey:@"product"];
    [userDefaults synchronize];
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
