//
//  introViewController.m
//  ScarCamo
//
//  Created by mac on 19/02/18.
//  Copyright © 2018 RohitMahajan. All rights reserved.
//

#import "introViewController.h"
#import "landingViewController.h"
#import "tabbarViewController.h"
@interface introViewController ()<UIScrollViewDelegate>
{
    UIScrollView *scrv;
    UIPageControl *pageControl;
}
@end

@implementation introViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden=YES;
    
    UIView *d=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,  self.view.frame.size.height)];
    d.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:d];
    
    
    scrv=[[UIScrollView alloc]initWithFrame:CGRectMake(0,0,self.view.frame.size.width, self.view.frame.size.height)];
    scrv.backgroundColor=[UIColor clearColor];
    scrv.delegate=self;
    scrv.pagingEnabled=YES;

    [self.view addSubview:scrv];
    scrv.contentSize=CGSizeMake(self.view.frame.size.width*6, 20);
    
    UIView *v=[[UIView alloc]initWithFrame:CGRectMake(0, 0,self.view.frame.size.width*5, self.view.frame.size.height/2)];
    v.backgroundColor=[UIColor whiteColor];
    v.alpha=0.6;
    [scrv addSubview:v];
    
   
    
    int x=0;
    for(int i=0;i<6;i++)
    {
        UIImageView *backImage=[[UIImageView alloc]initWithFrame:CGRectMake(x, 0, self.view.frame.size.width, self.view.frame.size.height)];
        [backImage setContentMode: UIViewContentModeScaleAspectFill];
        if(i==0)
        backImage.image=[UIImage imageNamed:@"s1"];
        else if(i==1)
            backImage.image=[UIImage imageNamed:@"s2"];
        else if(i==2)
            backImage.image=[UIImage imageNamed:@"s3"];
        else if(i==3)
            backImage.image=[UIImage imageNamed:@"s4"];
        else if(i==4)
            backImage.image=[UIImage imageNamed:@"s5"];
        else if(i==5)
            backImage.image=[UIImage imageNamed:@"s6"];
        [scrv addSubview:backImage];
        
        x=x+self.view.frame.size.width;
        
        UIView *backv=[[UIView alloc]initWithFrame:CGRectMake((self.view.frame.size.width*i), 0, scrv.frame.size.width, 80)];
        backv.backgroundColor=[UIColor darkGrayColor];
        
        UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(20, 25, scrv.frame.size.width-40, 55)];
        lbl.textColor=[UIColor whiteColor];
        lbl.numberOfLines=2;
        lbl.textAlignment=NSTextAlignmentCenter;
        lbl.font=[UIFont fontWithName:@"Helvetica" size:18];
        if(i==4)
        {
            [scrv addSubview: backv];

        lbl.text=@"Scanning Is Easy";
        [backv addSubview:lbl];
        }
        if(i==5)
        {
            [scrv addSubview: backv];

            lbl.text=@"Scan 2-3 Inches Away From Skin In Indoor Light, Rather Than Direct Sunlight";
            [backv addSubview:lbl];
        }
        UILabel *lbl1 = [[UILabel alloc]initWithFrame:CGRectMake((self.view.frame.size.width*i)+25,self.view.frame.size.height/2+ 125, scrv.frame.size.width-50, 40)];
        lbl1.textColor=[UIColor blackColor];
        lbl1.numberOfLines=2;
        lbl1.textAlignment=NSTextAlignmentCenter;
        lbl1.font=[UIFont fontWithName:@"Helvetica" size:18];
        lbl1.text=@"Lorem ipsum dolor sit amet. Consectetur adipiscing elit";
        //[scrv addSubview:lbl1];
        
 
    }
    UIButton *skip = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    skip.tag = 102;
    skip.frame = CGRectMake(self.view.frame.size.width/2-40,self.view.frame.size.height-65, 80, 35);
    [skip setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [skip setTitle:@"SKIP" forState:UIControlStateNormal];
    skip.layer.borderColor=[UIColor greenColor].CGColor;
    skip.layer.borderWidth=1.0;
    skip.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
    [skip addTarget:self action:@selector(skip_Action:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:skip];
    
    pageControl = [[UIPageControl alloc] init];
    pageControl.frame = CGRectMake(self.view.frame.size.width/2-30, skip.frame.origin.y+45,60, 20);
    pageControl.numberOfPages = 6;
    pageControl.tintColor=[UIColor blackColor];
    pageControl.pageIndicatorTintColor=[UIColor blackColor];
    pageControl.currentPage = 0;
    [self.view addSubview:pageControl];
    
    // Do any additional setup after loading the view.
}
- (void)scrollViewDidScroll:(UIScrollView *)_scrollView{
    
    CGFloat viewWidth = _scrollView.frame.size.width;
    // content offset - tells by how much the scroll view has scrolled.
    
    int pageNumber = floor((_scrollView.contentOffset.x - viewWidth/50) / viewWidth) +1;
    
    pageControl.currentPage=pageNumber;
    
}
-(void)next_Action:(UIButton*)btn
{
    scrv.contentOffset=CGPointMake(scrv.contentOffset.x+self.view.frame.size.width, scrv.contentOffset.y);
}
-(void)skip_Action:(UIButton*)btn
{
    tabbarViewController *smvc = [[tabbarViewController alloc]init];
    [[[UIApplication sharedApplication] delegate] window].rootViewController=smvc;

    //[self.navigationController pushViewController:smvc animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
