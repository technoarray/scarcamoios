//
//  changeAddressViewController.m
//  ScarCamo
//
//  Created by mac on 28/03/18.
//  Copyright © 2018 RohitMahajan. All rights reserved.
//

#import "changeAddressViewController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"

@interface changeAddressViewController ()<UITextFieldDelegate>
{
    UIButton *saveButton;
    UIScrollView *scrv;
    
    UITextField *fullName;
    UITextField *mobileNo;
    UITextField *ad1;
    UITextField *ad2;
    UITextField *city;
    UITextField *pincode;
    UITextField *state;
    MBProgressHUD *HUD;

}
@end

@implementation changeAddressViewController
-(void)backAction:(UIButton*)btn
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    
    UIView *navView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:navView];
    
    
    UIImageView *logo=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-68.5, 32, 137, 23)];
    logo.image=[UIImage imageNamed:@"logo1"];
    logo.contentMode = UIViewContentModeScaleAspectFit;
    [navView addSubview:logo];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    UIImage *img = [[UIImage imageNamed:@"back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(10, 20, 44, 44);
    [backButton setImage:img forState:UIControlStateNormal];
    [navView addSubview:backButton];
    
    UIView *v=[[UIView alloc]initWithFrame:CGRectMake(0, 63, self.view.frame.size.width, 1)];
    v.backgroundColor=[UIColor lightGrayColor];
    v.alpha=0.5;
    [navView addSubview:v];
    
    
    
    
    
    scrv=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64-30)];
    scrv.contentSize=CGSizeMake(0, self.view.frame.size.height+50);
    [self.view addSubview:scrv];
    
    UILabel *nameLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 5, self.view.frame.size.width, 30)];
    nameLbl.text=@"Please enter your shipping address";
    nameLbl.font = [UIFont fontWithName:@"Helvetica" size:18];
    nameLbl.textColor=[UIColor blackColor];
    nameLbl.textAlignment=NSTextAlignmentCenter;
    [scrv addSubview:nameLbl];
    
    
    int y= 50;
    fullName= [[UITextField alloc]initWithFrame:CGRectMake(20, y, self.view.frame.size.width-40, 40)];
    fullName.placeholder=@"Full Name";
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    fullName.leftView = paddingView;
    fullName.leftViewMode = UITextFieldViewModeAlways;
    fullName.layer.borderColor=[UIColor lightGrayColor].CGColor;
    fullName.layer.borderWidth=1.0;
    fullName.delegate=self;
    [scrv addSubview:fullName];
    
    y=y+50;
    
    mobileNo= [[UITextField alloc]initWithFrame:CGRectMake(20,y , self.view.frame.size.width-40, 40)];
    mobileNo.placeholder=@"Mobile No.";
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    mobileNo.leftView = paddingView1;
    mobileNo.leftViewMode = UITextFieldViewModeAlways;
    mobileNo.layer.borderColor=[UIColor lightGrayColor].CGColor;
    mobileNo.layer.borderWidth=1.0;
    mobileNo.delegate=self;
    [scrv addSubview:mobileNo];
    
    y=y+50;
    
    ad1= [[UITextField alloc]initWithFrame:CGRectMake(20,y , self.view.frame.size.width-40, 40)];
    ad1.placeholder=@"Address Line 1";
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    ad1.leftView = paddingView2;
    ad1.leftViewMode = UITextFieldViewModeAlways;
    ad1.layer.borderColor=[UIColor lightGrayColor].CGColor;
    ad1.layer.borderWidth=1.0;
    ad1.delegate=self;
    [scrv addSubview:ad1];
    
    y=y+50;
    
    ad2= [[UITextField alloc]initWithFrame:CGRectMake(20,y , self.view.frame.size.width-40, 40)];
    ad2.placeholder=@"Address Line 2";
    UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    ad2.leftView = paddingView3;
    ad2.leftViewMode = UITextFieldViewModeAlways;
    ad2.layer.borderColor=[UIColor lightGrayColor].CGColor;
    ad2.layer.borderWidth=1.0;
    ad2.delegate=self;
    [scrv addSubview:ad2];
    
    y=y+50;
    
    city= [[UITextField alloc]initWithFrame:CGRectMake(20,y , self.view.frame.size.width-40, 40)];
    city.placeholder=@"City";
    UIView *paddingView4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    city.leftView = paddingView4;
    city.leftViewMode = UITextFieldViewModeAlways;
    city.layer.borderColor=[UIColor lightGrayColor].CGColor;
    city.layer.borderWidth=1.0;
    city.delegate=self;
    [scrv addSubview:city];
    
    y=y+50;
    
    pincode= [[UITextField alloc]initWithFrame:CGRectMake(20,y , self.view.frame.size.width-40, 40)];
    pincode.placeholder=@"Pin Code";
    UIView *paddingView5 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    pincode.leftView = paddingView5;
    pincode.leftViewMode = UITextFieldViewModeAlways;
    pincode.layer.borderColor=[UIColor lightGrayColor].CGColor;
    pincode.layer.borderWidth=1.0;
    pincode.delegate=self;
    [scrv addSubview:pincode];
    
    y=y+50;
    
    state= [[UITextField alloc]initWithFrame:CGRectMake(20,y , self.view.frame.size.width-40, 40)];
    state.placeholder=@"State";
    UIView *paddingView6 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    state.leftView = paddingView6;
    state.leftViewMode = UITextFieldViewModeAlways;
    state.layer.borderColor=[UIColor lightGrayColor].CGColor;
    state.layer.borderWidth=1.0;
    state.delegate=self;
    [scrv addSubview:state];
    
     y=y+60;
    
    saveButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [saveButton addTarget:self action:@selector(stripeCall) forControlEvents:UIControlEventTouchUpInside];
    [saveButton setTitle:@"Save" forState:UIControlStateNormal];
    saveButton.frame = CGRectMake(self.view.frame.size.width/2-100, y, 200, 40);
    saveButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
    [saveButton setBackgroundImage:[UIImage imageNamed:@"btn"] forState:UIControlStateNormal];
    [saveButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [scrv addSubview:saveButton];
    
    [self getshippingAdrees];
    self.navigationController.navigationBarHidden=true;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)getshippingAdrees
{
    HUD=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText=@"Please Wait";
    NSString *st=[NSString stringWithFormat:@"http://18.219.134.84/slim_api/public/shipping_address/%@", [[NSUserDefaults standardUserDefaults]valueForKey:@"userid"]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:st parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@,    %@", responseObject,st);
        HUD.hidden=true;
        
        if([[responseObject valueForKey:@"code"]intValue]==0)
        {
            
        }
        if([[responseObject valueForKey:@"code"]intValue]==1)
        {
            
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:[responseObject valueForKey:@"result"] forKey:@"adressssssss"];
            [userDefaults synchronize];
            
            NSDictionary *dict=[responseObject valueForKey:@"result"];
            
            fullName.text=[dict valueForKey:@"full_name"];
            mobileNo.text=[dict valueForKey:@"mobile_number"];
            ad1.text=[dict valueForKey:@"address_line1"];
            ad2.text=[dict valueForKey:@"address_line2"];
            city.text=[dict valueForKey:@"city"];
            pincode.text=[dict valueForKey:@"pin_code"];
            state.text=[dict valueForKey:@"state"];
            
        }
        
        
        
        
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    //
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    //    [scrv setContentOffset:CGPointMake(0, 0) animated:YES];
    [textField resignFirstResponder];
    return YES;
}
-(void)stripeCall
{
    
    
    //url:
    //
    
    
    HUD=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText=@"Please Wait";
    NSDictionary *params = @{@"uid"                 : [[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                             @"full_name"           :fullName.text,
                             @"mobile_number"       : mobileNo.text,
                             @"pin_code"            : pincode.text,
                             @"address_line1"       : ad1.text,
                             @"address_line2"       : ad2.text,
                             @"city"                : city.text,
                             @"state"               : state.text,
                             };
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [manager POST:@"http://18.219.134.84/slim_api/public/shipping_address/edit" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"success!%@",[[responseObject valueForKey:@"result"][0]valueForKey:@"id"]);
        HUD.hidden=true;
        if([[responseObject valueForKey:@"code"]intValue]==1)
        {
            
            
            [self.navigationController popViewControllerAnimated:YES];
            
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                                message:[responseObject valueForKey:@"message"]
                                                               delegate:nil
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil];
            [alertView show];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
    }];
    
    
}
@end
