//
//  thankYouScreen.m
//  ScarCamo
//
//  Created by mac on 03/05/18.
//  Copyright © 2018 RohitMahajan. All rights reserved.
//

#import "thankYouScreen.h"

@interface thankYouScreen ()

@end

@implementation thankYouScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tabBarController.tabBar setHidden:true];

    
    UIImageView *productBg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height+100)];
    productBg.image=[UIImage imageNamed:@"productBg"];
    productBg.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:productBg];
    
    UIView *navView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:navView];
    
    self.navigationController.navigationBarHidden=true;
    
    UIImageView *logo=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-68.5, 32, 137, 23)];
    logo.image=[UIImage imageNamed:@"logo1"];
    logo.contentMode = UIViewContentModeScaleAspectFit;
    [navView addSubview:logo];
    
    
    UIView *v=[[UIView alloc]initWithFrame:CGRectMake(0, 63, self.view.frame.size.width, 1)];
    v.backgroundColor=[UIColor lightGrayColor];
    v.alpha=0.5;
    [navView addSubview:v];
    
    
    
    UILabel *nameLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height/2-30, self.view.frame.size.width, 30)];
    nameLbl.font = [UIFont fontWithName:@"Helvetica" size:20];
    nameLbl.textColor=[UIColor blackColor];
    nameLbl.textAlignment=NSTextAlignmentCenter;
    nameLbl.text=@"THANKS YOU! ";
    [self.view addSubview:nameLbl];
    
    UIView *v1=[[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height/2, self.view.frame.size.width, 1)];
    v1.backgroundColor=[UIColor lightGrayColor];
    v1.alpha=0.5;
    [self.view addSubview:v1];
    
    UILabel *discLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height/2+10,self.view.frame.size.width , 30)];
    discLbl.font = [UIFont fontWithName:@"Helvetica" size:16];
    discLbl.textColor=[UIColor blackColor];
    discLbl.text=@"YOUR ORDER IS IN PROCESS";
    discLbl.numberOfLines=5;
    discLbl.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:discLbl];
    
    UIView *v2=[[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height/2, self.view.frame.size.width+30, 1)];
    v2.backgroundColor=[UIColor lightGrayColor];
    v2.alpha=0.5;
    [self.view addSubview:v2];
    
    
    UIButton *next = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    next.frame = CGRectMake(self.view.frame.size.width/2-50,self.view.frame.size.height-90, 100, 40);
    [next setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [next setTitle:@"NEXT" forState:UIControlStateNormal];
    [next setBackgroundImage:[UIImage imageNamed:@"btn"] forState:UIControlStateNormal];
    next.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:16];
    [next addTarget:self action:@selector(next_Action) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:next];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.tabBarController.tabBar setHidden:false];
}
-(void)next_Action
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
