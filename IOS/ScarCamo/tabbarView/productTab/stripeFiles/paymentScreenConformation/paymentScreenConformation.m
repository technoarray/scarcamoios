//
//  paymentScreenConformation.m
//  ScarCamo
//
//  Created by mac on 03/05/18.
//  Copyright © 2018 RohitMahajan. All rights reserved.
//

#import "paymentScreenConformation.h"
#import <Stripe/Stripe.h>
#import "CardExampleViewController.h"
#import "BrowseExamplesViewController.h"
#import "AsyncImageView.h"
#import "changeAddressViewController.h"
#import "MBProgressHUD.h"

@interface paymentScreenConformation ()<STPPaymentCardTextFieldDelegate,UIScrollViewDelegate>

@property (weak, nonatomic) STPPaymentCardTextField *paymentTextField;
@property (weak, nonatomic) UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) UIScrollView *scrollView;
@end

@implementation paymentScreenConformation
{
    MBProgressHUD *HUD;

    UILabel *addressDetailLbl,*addressDetailLbl1,*addressDetailLbl2,*addressDetailLbl3;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];// this will do the trick
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:NO];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView *v1=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    v1.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:v1];
    
    UIView *navView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:navView];
    
    self.navigationController.navigationBarHidden=true;
    
    UIImageView *logo=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-68.5, 32, 137, 23)];
    logo.image=[UIImage imageNamed:@"logo1"];
    logo.contentMode = UIViewContentModeScaleAspectFit;
    [navView addSubview:logo];
    
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(10, 20, 44, 44);
    UIImage *img = [[UIImage imageNamed:@"back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];

    [backButton setImage:img forState:UIControlStateNormal];
    [navView addSubview:backButton];
    
    
    UIView *v=[[UIView alloc]initWithFrame:CGRectMake(0, 63, self.view.frame.size.width, 1)];
    v.backgroundColor=[UIColor lightGrayColor];
    v.alpha=0.5;
    [navView addSubview:v];
    
    
    
    UIScrollView *scrv=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 65, self.view.frame.size.width, self.view.frame.size.height-65)];
    scrv.backgroundColor=[UIColor whiteColor];
    scrv.contentSize=CGSizeMake(10, self.view.frame.size.height);
    scrv.delegate=self;
    [v1 addSubview: scrv];
    
    UIView *downView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width+2,160)];
    downView.backgroundColor=[UIColor whiteColor];
    downView.layer.borderColor=[UIColor whiteColor].CGColor;
    downView.layer.borderWidth=0.5;
    [scrv addSubview:downView];
    
    NSDictionary *dict=[[NSUserDefaults standardUserDefaults]valueForKey:@"producttttt"];
    
    
    AsyncImageView* productImage=[[AsyncImageView alloc]initWithFrame:CGRectMake(0, 5, 70, 120)];
    productImage.contentMode = UIViewContentModeScaleAspectFit;
    productImage.backgroundColor=[UIColor clearColor];
    productImage.imageURL=[NSURL URLWithString:[dict  valueForKey:@"prod_image"]];
    [downView addSubview:productImage];
    
    UILabel *nameLbl=[[UILabel alloc]initWithFrame:CGRectMake(70, 0, downView.frame.size.width-80, 30)];
    nameLbl.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    nameLbl.textColor=[UIColor blackColor];
    nameLbl.textAlignment=NSTextAlignmentLeft;
    nameLbl.text=[dict valueForKey:@"name"];
    [downView addSubview:nameLbl];
    
    UILabel *discLbl=[[UILabel alloc]initWithFrame:CGRectMake(70, 35, downView.frame.size.width-110, 70)];
    discLbl.font = [UIFont fontWithName:@"HelveticaNeue-Italic" size:15];
    discLbl.textColor=[UIColor blackColor];
    discLbl.text=[dict valueForKey:@"description"];
    discLbl.numberOfLines=2;
    discLbl.textAlignment=NSTextAlignmentLeft;
    [downView addSubview:discLbl];
    
    UIView *vaaa=[[UIView alloc]initWithFrame:CGRectMake(20, 115, downView.frame.size.width-40, 1)];
    vaaa.backgroundColor=[UIColor lightGrayColor];
    vaaa.alpha=0.5;
    [downView addSubview:vaaa];
    
    
    UILabel *priceLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 120, downView.frame.size.width, 40)];
    priceLbl.font = [UIFont fontWithName:@"HelveticaNeue-BoldItalic" size:22];
    priceLbl.textColor=[UIColor blackColor];
    priceLbl.text=[NSString stringWithFormat:@"Total: $%@",[dict valueForKey:@"price"]];
    priceLbl.textAlignment=NSTextAlignmentCenter;
    [downView addSubview:priceLbl];
    
    UILabel *addressLbl=[[UILabel alloc]initWithFrame:CGRectMake(20, downView.frame.size.height+downView.frame.origin.y+10, self.view.frame.size.width-110, 25)];
    addressLbl.text=@"Shipping Address";
    addressLbl.font = [UIFont fontWithName:@"Helvetica" size:19];
    addressLbl.textColor=[UIColor blackColor];
    addressLbl.textAlignment=NSTextAlignmentLeft;
    [scrv addSubview:addressLbl];
    
    
    addressDetailLbl=[[UILabel alloc]initWithFrame:CGRectMake(10, downView.frame.size.height+downView.frame.origin.y+35, self.view.frame.size.width-180, 20)];
    addressDetailLbl.font = [UIFont fontWithName:@"Helvetica" size:16];
    addressDetailLbl.textColor=[UIColor colorWithRed:144/255.0 green:144/255.0 blue:144/255.0 alpha:1.0];
    addressDetailLbl.numberOfLines=5.0;
    addressDetailLbl.textAlignment=NSTextAlignmentLeft;
    [scrv addSubview:addressDetailLbl];
    
    addressDetailLbl1=[[UILabel alloc]initWithFrame:CGRectMake(10, downView.frame.size.height+downView.frame.origin.y+55, self.view.frame.size.width-180, 20)];
    addressDetailLbl1.font = [UIFont fontWithName:@"Helvetica" size:16];
    addressDetailLbl1.textColor=[UIColor colorWithRed:144/255.0 green:144/255.0 blue:144/255.0 alpha:1.0];
    addressDetailLbl1.numberOfLines=5.0;
    addressDetailLbl1.textAlignment=NSTextAlignmentLeft;
    [scrv addSubview:addressDetailLbl1];
    
    addressDetailLbl2=[[UILabel alloc]initWithFrame:CGRectMake(10, downView.frame.size.height+downView.frame.origin.y+75, self.view.frame.size.width-180, 20)];
    addressDetailLbl2.font = [UIFont fontWithName:@"Helvetica" size:16];
    addressDetailLbl2.textColor=[UIColor colorWithRed:144/255.0 green:144/255.0 blue:144/255.0 alpha:1.0];
    addressDetailLbl2.numberOfLines=5.0;
    addressDetailLbl2.textAlignment=NSTextAlignmentLeft;
    [scrv addSubview:addressDetailLbl2];
    
    addressDetailLbl3=[[UILabel alloc]initWithFrame:CGRectMake(10, downView.frame.size.height+downView.frame.origin.y+95, self.view.frame.size.width-180, 20)];
    addressDetailLbl3.font = [UIFont fontWithName:@"Helvetica" size:16];
    addressDetailLbl3.textColor=[UIColor colorWithRed:144/255.0 green:144/255.0 blue:144/255.0 alpha:1.0];
    addressDetailLbl3.numberOfLines=5.0;
    addressDetailLbl3.textAlignment=NSTextAlignmentLeft;
    [scrv addSubview:addressDetailLbl3];
    
    
    UIButton *editAddress = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [editAddress addTarget:self action:@selector(changeaddressAction:) forControlEvents:UIControlEventTouchUpInside];
    [editAddress setTitle:@"Change Address" forState:UIControlStateNormal];
    editAddress.frame = CGRectMake(self.view.frame.size.width-110, downView.frame.size.height+downView.frame.origin.y+40, 100, 40);
    editAddress.layer.borderColor=[UIColor greenColor].CGColor;
    editAddress.layer.borderWidth=0.5;
    [editAddress setBackgroundImage:[UIImage imageNamed:@"btn"] forState:UIControlStateNormal];

//    editAddress.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    editAddress.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:12];
    [editAddress setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [scrv addSubview:editAddress];
    
    
    UIButton *payButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [payButton addTarget:self action:@selector(pay) forControlEvents:UIControlEventTouchUpInside];
    [payButton setTitle:@"PAY" forState:UIControlStateNormal];
    [payButton setBackgroundImage:[UIImage imageNamed:@"btn"] forState:UIControlStateNormal];
    payButton.frame = CGRectMake(self.view.frame.size.width/2-50, scrv.frame.size.height-120, 100, 40);
    payButton.layer.borderColor=[UIColor greenColor].CGColor;
    payButton.layer.borderWidth=0.5;
    //    editAddress.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    payButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
    [payButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [scrv addSubview:payButton];
    
    
    self.view.backgroundColor=[UIColor whiteColor];

    
   
    
    
    
    UIView *downView1 = [[UIView alloc]initWithFrame:CGRectMake(0, downView.frame.size.height+downView.frame.origin.y+125, self.view.frame.size.width+2,60)];
    downView1.backgroundColor=[UIColor colorWithRed:211/255.0 green:211/255.0 blue:211/255.0 alpha:0.4];
    downView1.layer.borderColor=[UIColor lightGrayColor].CGColor;
   // downView1.layer.borderWidth=0.5;
    [scrv addSubview:downView1];
    
    
    STPPaymentCardTextField *paymentTextField = [[STPPaymentCardTextField alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 40)];
    paymentTextField.delegate = self;
    paymentTextField.borderWidth=0;
    paymentTextField.cursorColor = [UIColor purpleColor];
    paymentTextField.postalCodeEntryEnabled = false;
    self.paymentTextField = paymentTextField;
    [downView1 addSubview:paymentTextField];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.hidesWhenStopped = YES;
    activityIndicator.frame=CGRectMake(self.view.frame.size.width-50, downView.frame.size.height+downView.frame.origin.y+180, 30, 30);
    self.activityIndicator = activityIndicator;
    [scrv addSubview:activityIndicator];
    
    
    // Do any additional setup after loading the view.
}
-(void)backAction:(UIButton*)btn
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)viewDidAppear:(BOOL)animated
{
    
    NSDictionary *dict=[[NSUserDefaults standardUserDefaults]valueForKey:@"adressssssss"];

    addressDetailLbl.text=[dict valueForKey:@"full_name"];
    
    addressDetailLbl1.text=[dict valueForKey:@"address_line1"];
    
    addressDetailLbl2.text=[dict valueForKey:@"address_line2"];
    
    addressDetailLbl3.text=[NSString stringWithFormat:@"%@,%@,%@",[dict valueForKey:@"city"],[dict valueForKey:@"state"],[dict valueForKey:@"pin_code"]];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)pay {
    if (![self.paymentTextField isValid]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                            message:@"Please enter valid card detail"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
        return;
    }
    if (![Stripe defaultPublishableKey]) {
        [self.delegate exampleViewController:self didFinishWithMessage:@"Please set a Stripe Publishable Key in Constants.m"];
        return;
    }
    [self.activityIndicator startAnimating];
    [[STPAPIClient sharedClient] createTokenWithCard:self.paymentTextField.cardParams
                                          completion:^(STPToken *token, NSError *error) {
                                              if (error) {
                                                  [self.delegate exampleViewController:self didFinishWithError:error];
                                              }
                                              [self.delegate createBackendChargeWithSource:token.tokenId completion:^(STPBackendChargeResult result, NSError *error) {
                                                  if (error) {
                                                      [self.delegate exampleViewController:self didFinishWithError:error];
                                                      return;
                                                  }
                                                  [self.delegate exampleViewController:self didFinishWithMessage:@"Payment successfully created"];
                                              }];
                                          }];
}

-(void)changeaddressAction:(UIButton*)btn
{
    changeAddressViewController  *smvc = [[changeAddressViewController alloc]init];
    [self.navigationController pushViewController:smvc animated:YES];
}
@end
