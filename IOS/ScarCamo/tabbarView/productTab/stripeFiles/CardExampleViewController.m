//
//  CardExampleViewController.m
//  Custom Integration (ObjC)
//
//  Created by Ben Guo on 2/22/17.
//  Copyright © 2017 Stripe. All rights reserved.
//

#import <Stripe/Stripe.h>
#import "CardExampleViewController.h"
#import "BrowseExamplesViewController.h"
#import "AsyncImageView.h"
/**
 This example demonstrates creating a payment with a credit/debit card. It creates a token
 using card information collected with STPPaymentCardTextField, and then sends the token
 to our example backend to create the charge request.
 */
@interface CardExampleViewController () <STPPaymentCardTextFieldDelegate, UIScrollViewDelegate>
@property (weak, nonatomic) STPPaymentCardTextField *paymentTextField;
@property (weak, nonatomic) UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) UIScrollView *scrollView;
@end

@implementation CardExampleViewController

- (void)loadView {
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectZero];
    scrollView.delegate = self;
    scrollView.alwaysBounceVertical = YES;
    scrollView.backgroundColor = [UIColor whiteColor];
    self.view = scrollView;
    self.scrollView = scrollView;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    //self.title = @"Card";
    //self.edgesForExtendedLayout = UIRectEdgeLeft;

//    UIBarButtonItem *buyButton = [[UIBarButtonItem alloc] initWithTitle:@"Pay" style:UIBarButtonItemStyleDone target:self action:@selector(pay)];
//    buyButton.enabled = NO;
//    self.navigationItem.rightBarButtonItem = buyButton;

    UIView *navView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:navView];
    
    self.navigationController.navigationBarHidden=true;

    UIImageView *logo=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-68.5, 32, 137, 23)];
    logo.image=[UIImage imageNamed:@"logo1"];
    logo.contentMode = UIViewContentModeScaleAspectFit;
    [navView addSubview:logo];
    
    
    UIView *v=[[UIView alloc]initWithFrame:CGRectMake(0, 63, self.view.frame.size.width, 1)];
    v.backgroundColor=[UIColor lightGrayColor];
    v.alpha=0.5;
    [navView addSubview:v];
    
    UIView *downView = [[UIView alloc]initWithFrame:CGRectMake(0, 65, self.view.frame.size.width+2,120)];
    downView.backgroundColor=[UIColor colorWithRed:235/255.0 green:229/255.0 blue:209/255.0 alpha:1.0];
    downView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    downView.layer.borderWidth=0.5;
    [self.view addSubview:downView];
    
    NSDictionary *dict=[[NSUserDefaults standardUserDefaults]valueForKey:@"producttttt"];

    
    AsyncImageView* productImage=[[AsyncImageView alloc]initWithFrame:CGRectMake(10, 5, 40, 70)];
    productImage.contentMode = UIViewContentModeScaleAspectFit;
    productImage.backgroundColor=[UIColor clearColor];
    productImage.imageURL=[NSURL URLWithString:[dict  valueForKey:@"prod_image"]];
    [downView addSubview:productImage];
    
    UILabel *nameLbl=[[UILabel alloc]initWithFrame:CGRectMake(80, 0, downView.frame.size.width-80, 30)];
    nameLbl.font = [UIFont fontWithName:@"Helvetica" size:16];
    nameLbl.textColor=[UIColor blackColor];
    nameLbl.textAlignment=NSTextAlignmentCenter;
    nameLbl.text=[dict valueForKey:@"name"];
    [downView addSubview:nameLbl];
    
    UILabel *discLbl=[[UILabel alloc]initWithFrame:CGRectMake(80, 35, downView.frame.size.width-80, 80)];
    discLbl.font = [UIFont fontWithName:@"Helvetica" size:12];
    discLbl.textColor=[UIColor blackColor];
    discLbl.text=[dict valueForKey:@"description"];
    discLbl.numberOfLines=5;
    discLbl.textAlignment=NSTextAlignmentCenter;
    [downView addSubview:discLbl];
    
    UIView *vaaa=[[UIView alloc]initWithFrame:CGRectMake(20, 115, downView.frame.size.width-40, 1)];
    vaaa.backgroundColor=[UIColor lightGrayColor];
    vaaa.alpha=0.5;
    [downView addSubview:vaaa];
    
    
    UILabel *priceLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 120, downView.frame.size.width, 20)];
    priceLbl.font = [UIFont fontWithName:@"Helvetica" size:15];
    priceLbl.textColor=[UIColor blackColor];
    priceLbl.text=[NSString stringWithFormat:@"Total: $%@",[dict valueForKey:@"price"]];
    priceLbl.textAlignment=NSTextAlignmentCenter;
    [downView addSubview:priceLbl];
    
    
    
    
    
    
    
    
    
    
    STPPaymentCardTextField *paymentTextField = [[STPPaymentCardTextField alloc] init];
    paymentTextField.delegate = self;
    paymentTextField.cursorColor = [UIColor purpleColor];
    paymentTextField.postalCodeEntryEnabled = false;
    self.paymentTextField = paymentTextField;
    [self.view addSubview:paymentTextField];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.hidesWhenStopped = YES;
    self.activityIndicator = activityIndicator;
    [self.view addSubview:activityIndicator];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    CGFloat padding = 15;
    CGFloat width = CGRectGetWidth(self.view.frame) - (padding*2);
    CGRect bounds = self.view.bounds;
    self.paymentTextField.frame = CGRectMake(padding, 185, width, 44);
    self.activityIndicator.center = CGPointMake(CGRectGetMidX(bounds),
                                                CGRectGetMaxY(self.paymentTextField.frame) + padding*2);
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.view.backgroundColor=[UIColor whiteColor];

    [self.paymentTextField becomeFirstResponder];
}

- (void)paymentCardTextFieldDidChange:(nonnull STPPaymentCardTextField *)textField {
    self.navigationItem.rightBarButtonItem.enabled = textField.isValid;
}


- (void)pay {
    if (![self.paymentTextField isValid]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                            message:@"Please enter valid card detail"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
        
        return;
    }
    if (![Stripe defaultPublishableKey]) {
        [self.delegate exampleViewController:self didFinishWithMessage:@"Please set a Stripe Publishable Key in Constants.m"];
        return;
    }
    [self.activityIndicator startAnimating];
    [[STPAPIClient sharedClient] createTokenWithCard:self.paymentTextField.cardParams
                                          completion:^(STPToken *token, NSError *error) {
                                              if (error) {
                                                  [self.delegate exampleViewController:self didFinishWithError:error];
                                              }
                                              [self.delegate createBackendChargeWithSource:token.tokenId completion:^(STPBackendChargeResult result, NSError *error) {
                                                  if (error) {
                                                      [self.delegate exampleViewController:self didFinishWithError:error];
                                                      return;
                                                  }
                                                  [self.delegate exampleViewController:self didFinishWithMessage:@"Payment successfully created"];
                                              }];
                                          }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:NO];
}

@end
