//
//  paymentDetailConformationView.m
//  ScarCamo
//
//  Created by mac on 23/03/18.
//  Copyright © 2018 RohitMahajan. All rights reserved.
//

#import "paymentDetailConformationView.h"
#import "shippingAddressViewController.h"
@interface paymentDetailConformationView ()
{
    UITextField *fullName;
    UITextField *mobileNo;
    UITextField *ad1;
    UITextField *ad2;
    UITextField *city;
    UITextField *pincode;
    UITextField *state;
}
@end

@implementation paymentDetailConformationView
-(void)backAction:(UIButton*)btn
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=true;

    
    UIView *navView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:navView];
    
    
    UIImageView *logo=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-68.5, 32, 137, 23)];
    logo.image=[UIImage imageNamed:@"logo1"];
    logo.contentMode = UIViewContentModeScaleAspectFit;
    [navView addSubview:logo];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    UIImage *img = [[UIImage imageNamed:@"back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(10, 20, 44, 44);
    [backButton setImage:img forState:UIControlStateNormal];
    [navView addSubview:backButton];
    
    UIView *v=[[UIView alloc]initWithFrame:CGRectMake(0, 63, self.view.frame.size.width, 1)];
    v.backgroundColor=[UIColor lightGrayColor];
    v.alpha=0.5;
    [navView addSubview:v];
    
    
    UILabel *username = [[UILabel alloc] initWithFrame:CGRectMake (20, 74,self.view.frame.size.width-40, 30)];
    [username setFont:[UIFont fontWithName:@"Helvetica" size:18]];
    username.textColor=[UIColor whiteColor];
    username.lineBreakMode=NSLineBreakByWordWrapping;
    username.numberOfLines=6;
    username.textAlignment=NSTextAlignmentLeft;
    username.backgroundColor=[UIColor clearColor];
    [username setText:@"Your Skin Tone Not Selected"];
    [self.view addSubview:username];
    
    UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake (20, 74,self.view.frame.size.width-40, 30)];
    [price setFont:[UIFont fontWithName:@"Helvetica" size:18]];
    price.textColor=[UIColor whiteColor];
    price.lineBreakMode=NSLineBreakByWordWrapping;
    price.numberOfLines=6;
    username.textAlignment=NSTextAlignmentLeft;
    price.backgroundColor=[UIColor clearColor];
    [price setText:@"Your Skin Tone Not Selected"];
    [self.view addSubview:price];
    
    
    
    UIScrollView *scrv=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64-95)];
    
    [self.view addSubview:scrv];
    
    int y= 10;
    fullName= [[UITextField alloc]initWithFrame:CGRectMake(20, y, self.view.frame.size.width-40, 40)];
    fullName.placeholder=@"Full Name";
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    fullName.leftView = paddingView;
    fullName.leftViewMode = UITextFieldViewModeAlways;
    fullName.layer.cornerRadius=5.0;
    fullName.layer.borderColor=[UIColor greenColor].CGColor;
    fullName.layer.borderWidth=1.0;
    fullName.delegate=self;
    [scrv addSubview:fullName];
    
    y=y+50;
    
    mobileNo= [[UITextField alloc]initWithFrame:CGRectMake(20,y , self.view.frame.size.width-40, 40)];
    mobileNo.placeholder=@"Mobile No.";
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    mobileNo.leftView = paddingView1;
    mobileNo.leftViewMode = UITextFieldViewModeAlways;
    mobileNo.layer.cornerRadius=5.0;
    mobileNo.layer.borderColor=[UIColor greenColor].CGColor;
    mobileNo.layer.borderWidth=1.0;
    mobileNo.delegate=self;
    [scrv addSubview:mobileNo];
    
    y=y+50;
    
    ad1= [[UITextField alloc]initWithFrame:CGRectMake(20,y , self.view.frame.size.width-40, 40)];
    ad1.placeholder=@"Address Line 1";
    ad1.layer.cornerRadius=5.0;
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    ad1.leftView = paddingView2;
    ad1.leftViewMode = UITextFieldViewModeAlways;
    ad1.layer.borderColor=[UIColor greenColor].CGColor;
    ad1.layer.borderWidth=1.0;
    ad1.delegate=self;
    [scrv addSubview:ad1];
    
    y=y+50;
    
    ad2= [[UITextField alloc]initWithFrame:CGRectMake(20,y , self.view.frame.size.width-40, 40)];
    ad2.placeholder=@"Address Line 2";
    UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    ad2.layer.cornerRadius=5.0;
    ad2.leftView = paddingView3;
    ad2.leftViewMode = UITextFieldViewModeAlways;
    ad2.layer.borderColor=[UIColor greenColor].CGColor;
    ad2.layer.borderWidth=1.0;
    ad2.delegate=self;
    [scrv addSubview:ad2];
    
    y=y+50;
    
    city= [[UITextField alloc]initWithFrame:CGRectMake(20,y , self.view.frame.size.width-40, 40)];
    city.placeholder=@"City";
    UIView *paddingView4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    city.leftView = paddingView4;
    city.leftViewMode = UITextFieldViewModeAlways;
    city.layer.cornerRadius=5.0;
    city.layer.borderColor=[UIColor greenColor].CGColor;
    city.layer.borderWidth=1.0;
    city.delegate=self;
    [scrv addSubview:city];
    
    y=y+50;
    
    pincode= [[UITextField alloc]initWithFrame:CGRectMake(20,y , self.view.frame.size.width-40, 40)];
    pincode.placeholder=@"Pin Code";
    pincode.layer.cornerRadius=5.0;
    UIView *paddingView5 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    pincode.leftView = paddingView5;
    pincode.leftViewMode = UITextFieldViewModeAlways;
    pincode.layer.borderColor=[UIColor greenColor].CGColor;
    pincode.layer.borderWidth=1.0;
    pincode.delegate=self;
    [scrv addSubview:pincode];
    
    y=y+50;
    
    state= [[UITextField alloc]initWithFrame:CGRectMake(20,y , self.view.frame.size.width-40, 40)];
    state.placeholder=@"State";
    state.layer.cornerRadius=5.0;
    UIView *paddingView6 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    state.leftView = paddingView6;
    state.leftViewMode = UITextFieldViewModeAlways;
    state.layer.borderColor=[UIColor greenColor].CGColor;
    state.layer.borderWidth=1.0;
    state.delegate=self;
    [scrv addSubview:state];
    
    
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)buyAction
{
//    NSDictionary *dict=[[NSUserDefaults standardUserDefaults]valueForKey:@"product"];
//price
    shippingAddressViewController *exampleVC = [shippingAddressViewController new];
    [self.navigationController pushViewController:exampleVC animated:YES];
  
}

@end
