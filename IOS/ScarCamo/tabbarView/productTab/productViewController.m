//
//  productViewController.m
//  ScarCamo
//
//  Created by mac on 25/02/18.
//  Copyright © 2018 RohitMahajan. All rights reserved.
//

#import "productViewController.h"
#import "AFNetworking.h"
#import "AsyncImageView.h"
#import <Stripe/Stripe.h>
#import "ShippingManager.h"
#import "paymentDetailConformationView.h"
#import "shippingAddressViewController.h"
#import "ViewController.h"

#import "chooseViewController.h"
#import "scanViewController.h"
#import "aboutUsViewController.h"
#import "contactUsViewController.h"
#import "videoViewController.h"
#import "historyViewController.h"
#import "myOrderViewController.h"
#import "notificationsViewController.h"


@interface productViewController ()<STPPaymentCardTextFieldDelegate,STPAddCardViewControllerDelegate,PKPaymentAuthorizationViewControllerDelegate>
{
    UILabel *skinTone;
    STPCustomerContext *customerContext;
    STPPaymentContext *paymentContext;
    
    AsyncImageView *productImage;
    UIView *downView;
    UILabel *nameLbl;
    UILabel *priceLbl;
    UIView *vaaa;
    UILabel *discLbl;
    UIView *v1;
    UIImageView *imgggg;
    UIImageView *imgggg1;
    UIButton *buyButton;
    
    UIView *menuV;
    UITableView *tableView;
    NSMutableArray *menuArray,*menuImageArray;
}
@property (nonatomic) ShippingManager *shippingManager;
@property (nonatomic) BOOL applePaySucceeded;
@property (nonatomic) NSError *applePayError;
@property (weak, nonatomic) STPPaymentCardTextField *paymentTextField;
@property (weak, nonatomic) UIActivityIndicatorView *activityIndicator;
@end

@implementation productViewController
-(void)onTick:(NSTimer *)timer {
    [self.tabBarController.tabBar setHidden:false];
}
-(void)viewWillAppear:(BOOL)animated
{
    [NSTimer scheduledTimerWithTimeInterval:0.5
                                     target:self
                                   selector:@selector(onTick:)
                                   userInfo:nil
                                    repeats:NO];
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"product"]);
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"product"] isEqualToString:@"yes"])
    {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:@"no" forKey:@"product"];
        [userDefaults synchronize];
        [self buyAction];
    }
    
   
    
    self.navigationController.navigationBarHidden=true;
    NSData *data =[[NSUserDefaults standardUserDefaults]valueForKey:@"color"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if(color==nil)
    {
        skinTone.backgroundColor=[UIColor lightGrayColor];
        skinTone.text=@"Your skin tone not selected";
    }
    else
    {
        skinTone.text=@"This is your skin tone";
        skinTone.backgroundColor=color;
    }
    [self getProduct];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *userid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    
    if(userid == (id)[NSNull null]|| userid.length == 0  || [userid isEqual:@"(null)"])
    {
        menuArray=[[NSMutableArray alloc]initWithObjects:@" MY PROFILE",@" MY ORDER",@" HISTORY",@" NOTIFICATIONS",@" PRODUCT",@" ABOUT US",@" CONTACT US", nil];
        menuImageArray=[[NSMutableArray alloc]initWithObjects:@"profile",@"mOrder",@"history",@"noti",@"prod",@"about",@"contactUs", nil];
    }
    else
    {
        menuArray=[[NSMutableArray alloc]initWithObjects:@" MY PROFILE",@" MY ORDER",@" HISTORY",@" NOTIFICATIONS",@" PRODUCT",@" ABOUT US",@" CONTACT US",@" LOGOUT", nil];
        menuImageArray=[[NSMutableArray alloc]initWithObjects:@"profile",@"mOrder",@"history",@"noti",@"prod",@"about",@"contactUs",@"logout", nil];
    }
    
    
    
    [self.tabBarController setSelectedIndex:1];
    [self.tabBarController.tabBar setHidden:false];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    UIView *navView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:navView];
    
    
    UIImageView *logo=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-68.5, 32, 137, 23)];
    logo.image=[UIImage imageNamed:@"logo1"];
    logo.contentMode = UIViewContentModeScaleAspectFit;
    [navView addSubview:logo];
   
    
    UIView *v=[[UIView alloc]initWithFrame:CGRectMake(0, 63, self.view.frame.size.width, 1)];
    v.backgroundColor=[UIColor lightGrayColor];
    v.alpha=0.5;
    [navView addSubview:v];

    UIImage *img = [[UIImage imageNamed:@"menu"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    menuButton.frame = CGRectMake(10,25, 40, 40);
    [menuButton setImage:img forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(menu_action:) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:menuButton];

    
    self.navigationController.navigationBarHidden=true;
    
    
    
    
    UIImageView *productBg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64-30)];
    productBg.image=[UIImage imageNamed:@"productBg"];
    productBg.contentMode = UIViewContentModeScaleAspectFill;
    //[self.view addSubview:productBg];
    
    
    
    skinTone=[[UILabel alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, 30)];
    [skinTone setFont:[UIFont fontWithName:@"Helvetica-Bold" size:12]];
    skinTone.textAlignment=NSTextAlignmentCenter;
    skinTone.textColor=[UIColor whiteColor];

    [self.view addSubview:skinTone];
    
    
    
    

    
    productImage=[[AsyncImageView alloc]initWithFrame:CGRectMake(20, 98, self.view.frame.size.width-40, self.view.frame.size.height/3)];
    productImage.contentMode = UIViewContentModeScaleAspectFit;
    productImage.backgroundColor=[UIColor clearColor];
    [self.view addSubview:productImage];
    
    
    downView = [[UIView alloc]initWithFrame:CGRectMake(20, 94+self.view.frame.size.height/3+20, self.view.frame.size.width-40, self.view.frame.size.height-(74+self.view.frame.size.height/3+20)-90)];
    downView.backgroundColor=[UIColor whiteColor];
    downView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    downView.layer.borderWidth=0.5;
    [self.view addSubview:downView];
    
    
    nameLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, downView.frame.size.width, 30)];
    nameLbl.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    nameLbl.textColor=[UIColor blackColor];
    nameLbl.textAlignment=NSTextAlignmentCenter;
    [downView addSubview:nameLbl];
    
    priceLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 25, downView.frame.size.width, 20)];
    priceLbl.font = [UIFont fontWithName:@"HelveticaNeue-BoldItalic" size:15];
    priceLbl.textColor=[UIColor blackColor];
    priceLbl.textAlignment=NSTextAlignmentCenter;
    [downView addSubview:priceLbl];
    
    vaaa=[[UIView alloc]initWithFrame:CGRectMake(20, 50, downView.frame.size.width-40, 1)];
    vaaa.backgroundColor=[UIColor lightGrayColor];
    vaaa.alpha=0.5;
    [downView addSubview:vaaa];
    
    
    discLbl=[[UILabel alloc]initWithFrame:CGRectMake(10, 52, downView.frame.size.width-20, 80)];
    discLbl.font = [UIFont fontWithName:@"HelveticaNeue-BoldItalic" size:12];
    if(self.view.frame.size.width>320)
        discLbl.font = [UIFont fontWithName:@"HelveticaNeue-BoldItalic" size:14];

    discLbl.textColor=[UIColor blackColor];
    discLbl.numberOfLines=5;
    discLbl.textAlignment=NSTextAlignmentCenter;
    [downView addSubview:discLbl];
    
    
    v1=[[UIView alloc]initWithFrame:CGRectMake(20, 134, downView.frame.size.width-40, 1)];
    v1.backgroundColor=[UIColor lightGrayColor];
    v1.alpha=0.5;
    [downView addSubview:v1];
    
    
    imgggg=[[UIImageView alloc]initWithFrame:CGRectMake(downView.frame.size.width/2-50, downView.frame.size.height-52, 100, 40)];
    [downView addSubview:imgggg];
    
    imgggg1=[[UIImageView alloc]initWithFrame:CGRectMake(downView.frame.size.width/2-7.5, downView.frame.size.height-57, 15, 15)];
    imgggg1.image=[UIImage imageNamed:@"cart"];
    imgggg1.backgroundColor=[UIColor whiteColor];
    
    buyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [buyButton addTarget:self action:@selector(buyAction) forControlEvents:UIControlEventTouchUpInside];
    [buyButton setTitle:@"BUY" forState:UIControlStateNormal];
    [buyButton setBackgroundImage:[UIImage imageNamed:@"btn"] forState:UIControlStateNormal];
    buyButton.frame = CGRectMake(downView.frame.size.width/2-50, downView.frame.size.height-55, 100, 40);
    buyButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
    [buyButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [downView addSubview:buyButton];
    
    //[downView addSubview:imgggg1];

}

- (void)menu_action:(UIButton*)btn
{
    [self addMenuView];
}

-(void)buyAction
{
    NSString *userid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    
    NSData *data =[[NSUserDefaults standardUserDefaults]valueForKey:@"color"];
    UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    
    if(color==nil)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                            message:@"Please select skin tone"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
        [self.tabBarController setSelectedIndex:1];
        
    }
    
    else if(userid == (id)[NSNull null]|| userid.length == 0  || [userid isEqual:@"(null)"])
    {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:@"yes" forKey:@"product"];
        [userDefaults synchronize];
        ViewController *controller = [[ViewController alloc]init];
        [self.navigationController pushViewController:controller animated:NO];
    }
    else
    {
    shippingAddressViewController *exampleVC = [shippingAddressViewController new];
    [self.navigationController pushViewController:exampleVC animated:YES];
    }

         
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getProduct
{
    NSString *st=[NSString stringWithFormat:@"http://18.219.134.84/slim_api/public/products/all/%@", [[NSUserDefaults standardUserDefaults]valueForKey:@"index"]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:st parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@,    %@", responseObject,st);
        
        
        productImage.imageURL =[NSURL URLWithString:[responseObject valueForKey:@"prod_image"]];
        nameLbl.text=[[responseObject valueForKey:@"result"][0]valueForKey:@"name"];
        priceLbl.text=[NSString stringWithFormat:@"$%@",[[responseObject valueForKey:@"result"][0]valueForKey:@"price"]];
        discLbl.text=[NSString stringWithFormat:@"$%@",[[responseObject valueForKey:@"result"][0]valueForKey:@"description"]];
       
        NSMutableDictionary *dict =[[responseObject valueForKey:@"result"][0] mutableCopy];
        [dict setValue:[responseObject valueForKey:@"prod_image"] forKey:@"prod_image"];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:dict forKey:@"producttttt"];
        [userDefaults synchronize];
        
        
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}


-(void)addMenuView
{
    menuV=[[UIView alloc]initWithFrame:CGRectMake(0, 24, self.view.frame.size.width, self.view.frame.size.height-24)];
    menuV.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    [[[[UIApplication sharedApplication] delegate] window] addSubview:menuV];
    
    UIView *v=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-60, self.view.frame.size.height-24)];
    v.backgroundColor=[UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0];
    [menuV addSubview:v];
    
    
    
    tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 120, v.frame.size.width, v.frame.size.height-120) style:UITableViewStylePlain];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [v addSubview:tableView];
    
    UILabel *skinTone = [[UILabel alloc] initWithFrame:CGRectMake (20, 30,v.frame.size.width-40, 70)];
    [skinTone setFont:[UIFont fontWithName:@"Helvetica" size:18]];
    skinTone.textColor=[UIColor colorWithRed:133/255.0 green:133/255.0 blue:133/255.0 alpha:1.0];
    skinTone.lineBreakMode=NSLineBreakByWordWrapping;
    skinTone.numberOfLines=6;
    skinTone.textAlignment=NSTextAlignmentCenter;
    skinTone.backgroundColor=[UIColor clearColor];
    
    if([[NSUserDefaults standardUserDefaults]valueForKey:@"username"])
    {
        [skinTone setText:[[NSUserDefaults standardUserDefaults]valueForKey:@"username"]];
    }
    else
        [skinTone setText:@""];
    
    [v addSubview:skinTone];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    backButton.frame = CGRectMake(menuV.frame.size.width-100, 10, 30, 30);
    [backButton setTitleColor:[UIColor colorWithRed:133/255.0 green:133/255.0 blue:133/255.0 alpha:1.0] forState:UIControlStateNormal];
    [backButton setTitle:@"" forState:UIControlStateNormal];
    UIImage *img = [[UIImage imageNamed:@"cross"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    [backButton setImage:img forState:UIControlStateNormal];
    backButton.layer.borderColor=[UIColor colorWithRed:133/255.0 green:133/255.0 blue:133/255.0 alpha:1.0].CGColor;
    backButton.layer.borderWidth=1.0;
    backButton.layer.cornerRadius=backButton.frame.size.width/2;
    backButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
    [backButton addTarget:self action:@selector(back_Action) forControlEvents:UIControlEventTouchUpInside];
    [menuV addSubview:backButton];
}
-(void)back_Action
{
    [menuV removeFromSuperview];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return menuArray.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CELL"];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    
    
    
    UIButton *changePass = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [changePass setTitle:[menuArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
    //    changePass.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    changePass.frame = CGRectMake(20, 10,tableView.frame.size.width-40, 20);
    changePass.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:13];
    UIImage *img2 = [[UIImage imageNamed:menuImageArray[indexPath.row]]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [changePass setImage:img2 forState:UIControlStateNormal];
    changePass.userInteractionEnabled=false;
    [changePass setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cell addSubview:changePass];
    
    
    //    UIFont * myFont =[UIFont fontWithName:@"Lato-Regular" size:12];
    //    CGRect labelFrame1 = CGRectMake (100, 5,tableView.frame.size.width-80, 30);
    //    UILabel *label1 = [[UILabel alloc] initWithFrame:labelFrame1];
    //    [label1 setFont:myFont];
    //    label1.textColor=[UIColor whiteColor];
    //    label1.textAlignment=NSTextAlignmentLeft;
    //    label1.backgroundColor=[UIColor clearColor];
    //    [label1 setText:[menuArray objectAtIndex:indexPath.row]];
    //    [cell addSubview:label1];
    //
    //    UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(77, 12, 16, 16)];
    //    img.image=[UIImage imageNamed:menuImageArray[indexPath.row]];
    //    [cell addSubview:img];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(50, 39, tableView.frame.size.width-100,0.3)];
    [view setBackgroundColor:[UIColor colorWithRed:255.0 green:255.0 blue:255.0 alpha:0.1]];
    [cell addSubview:view];
    
    cell.backgroundColor=[UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0)
    {
        self.tabBarController.selectedIndex=2;
        [self back_Action];
    }
    if(indexPath.row==7)
    {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:@"" forKey:@"userid"];
        [userDefaults setObject:@"" forKey:@"username"];
        [userDefaults setObject:nil forKey:@"color"];

        [userDefaults synchronize];
        self.tabBarController.selectedIndex=2;
        [self back_Action];
    }
    if(indexPath.row==4)
    {
        self.tabBarController.selectedIndex=0;
        [self back_Action];
    }
    if(indexPath.row==5)
    {
        aboutUsViewController *controller = [[aboutUsViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
        [self back_Action];
    }
    if(indexPath.row==6)
    {
        contactUsViewController *controller = [[contactUsViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
        [self back_Action];
    }
    //    if(indexPath.row==7)
    //    {
    //        videoViewController *controller = [[videoViewController alloc]init];
    //        [self.navigationController pushViewController:controller animated:YES];
    //        [self back_Action];
    //    }
    if(indexPath.row==2)
    {
        historyViewController *controller = [[historyViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
        [self back_Action];
    }
    if(indexPath.row==1)
    {
        myOrderViewController *controller = [[myOrderViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
        [self back_Action];
    }
    if(indexPath.row==3)
    {
        notificationsViewController *controller = [[notificationsViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
        [self back_Action];
    }
    
    
    
}

@end
