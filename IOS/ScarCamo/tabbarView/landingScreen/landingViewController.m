//
//  landingViewController.m
//  ScarCamo
//
//  Created by mac on 20/02/18.
//  Copyright © 2018 RohitMahajan. All rights reserved.
//

#import "landingViewController.h"
#import "chooseViewController.h"
#import "scanViewController.h"
#import "aboutUsViewController.h"
#import "contactUsViewController.h"
#import "videoViewController.h"
#import "historyViewController.h"
#import "myOrderViewController.h"
#import "notificationsViewController.h"

@interface landingViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UIView *menuV;
    UITableView *tableView;
    NSMutableArray *menuArray,*menuImageArray;
}
@end

@implementation landingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewappearrr) name:@"appear" object:nil];
//    NSDictionary *orientationData = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%@",@""] forKey:@"cat_id"];
//    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
//    [notificationCenter postNotificationName:@"catSelected" object:nil userInfo:orientationData];
    //@" 3D VIDEO"  @"video",
    
   
    [self.tabBarController.tabBar setHidden:false];
    self.navigationController.navigationBarHidden=true;
    self.view.backgroundColor=[UIColor whiteColor];
//    UIImageView *backImage=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    backImage.image=[UIImage imageNamed:@"landingbg"];
//    [self.view addSubview:backImage];
    
    
    UIView *navView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:navView];
    
    
    UIImageView *logo=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-68.5, 32, 137, 23)];
    logo.image=[UIImage imageNamed:@"logo1"];
    logo.contentMode = UIViewContentModeScaleAspectFit;
    [navView addSubview:logo];
    
    UIView *v=[[UIView alloc]initWithFrame:CGRectMake(0, 63, self.view.frame.size.width, 1)];
    v.backgroundColor=[UIColor lightGrayColor];
    v.alpha=0.5;
    [navView addSubview:v];

    
    UIImage *img = [[UIImage imageNamed:@"menu"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    menuButton.frame = CGRectMake(10,25, 40, 40);
    [menuButton setImage:img forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(menu_action:) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:menuButton];
   
    
//    UIImageView *logo=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-68.5, 40, 137, 23)];
//    logo.image=[UIImage imageNamed:@"logo1"];
//    logo.contentMode = UIViewContentModeScaleAspectFit;
//    [self.view addSubview:logo];
    
    
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-90, 75, 180, 120)];
    lbl.textColor=[UIColor blackColor];
    lbl.numberOfLines=2;
    lbl.textAlignment=NSTextAlignmentCenter;
    lbl.font=[UIFont fontWithName:@"Helvetica" size:25];
    lbl.text=@"DETECT YOUR SKIN TONE";
    [self.view addSubview:lbl];
    
    
    UIButton *scan = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    scan.tag = 102;
    scan.frame = CGRectMake(self.view.frame.size.width/2-100,self.view.frame.size.height/2-60, 200, 50);
    [scan setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [scan setTitle:@"SCAN" forState:UIControlStateNormal];
    [scan setBackgroundImage:[UIImage imageNamed:@"btn"] forState:UIControlStateNormal];
    scan.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [scan addTarget:self action:@selector(scan_Action:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:scan];
    
    UILabel *orlbl = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-40, self.view.frame.size.height/2+15, 80, 20)];
    orlbl.textColor=[UIColor blackColor];
    orlbl.numberOfLines=2;
    orlbl.textAlignment=NSTextAlignmentCenter;
    orlbl.font=[UIFont fontWithName:@"Helvetica" size:18];
    orlbl.text=@"- or -";
    [self.view addSubview:orlbl];
    
    UIButton *choose = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    choose.tag = 102;
    [choose setBackgroundImage:[UIImage imageNamed:@"btn"] forState:UIControlStateNormal];
    choose.frame = CGRectMake(self.view.frame.size.width/2-100,self.view.frame.size.height/2+60, 200, 50);
    [choose setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [choose setTitle:@"CHOOSE" forState:UIControlStateNormal];
    choose.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [choose addTarget:self action:@selector(choose_Action:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:choose];
}
-(void)viewDidAppear:(BOOL)animated
{
    NSString *userid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    
    if(userid == (id)[NSNull null]|| userid.length == 0  || [userid isEqual:@"(null)"])
    {
        menuArray=[[NSMutableArray alloc]initWithObjects:@" MY PROFILE",@" MY ORDER",@" HISTORY",@" NOTIFICATIONS",@" PRODUCT",@" ABOUT US",@" CONTACT US", nil];
        menuImageArray=[[NSMutableArray alloc]initWithObjects:@"profile",@"mOrder",@"history",@"noti",@"prod",@"about",@"contactUs", nil];
    }
    else
    {
        menuArray=[[NSMutableArray alloc]initWithObjects:@" MY PROFILE",@" MY ORDER",@" HISTORY",@" NOTIFICATIONS",@" PRODUCT",@" ABOUT US",@" CONTACT US",@" LOGOUT", nil];
        menuImageArray=[[NSMutableArray alloc]initWithObjects:@"profile",@"mOrder",@"history",@"noti",@"prod",@"about",@"contactUs",@"logout", nil];
    }
    
}
- (void)menu_action:(UIButton*)btn
{
    [self addMenuView];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)scan_Action:(UIButton*)btn
{
    scanViewController *smvc = [[scanViewController alloc]init];
    [self.navigationController pushViewController:smvc animated:YES];
}
-(void)choose_Action:(UIButton*)btn
{
    chooseViewController *smvc = [[chooseViewController alloc]init];
    [self.navigationController pushViewController:smvc animated:YES];
}
-(void)addMenuView
{
    menuV=[[UIView alloc]initWithFrame:CGRectMake(0, 24, self.view.frame.size.width, self.view.frame.size.height-24)];
    menuV.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    [[[[UIApplication sharedApplication] delegate] window] addSubview:menuV];
    
    UIView *v=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-60, self.view.frame.size.height-24)];
    v.backgroundColor=[UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0];
    [menuV addSubview:v];
    
    
    
    tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 120, v.frame.size.width, v.frame.size.height-120) style:UITableViewStylePlain];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    [v addSubview:tableView];
    
    UILabel *skinTone = [[UILabel alloc] initWithFrame:CGRectMake (20, 30,v.frame.size.width-40, 70)];
    [skinTone setFont:[UIFont fontWithName:@"Helvetica" size:18]];
    skinTone.textColor=[UIColor colorWithRed:133/255.0 green:133/255.0 blue:133/255.0 alpha:1.0];
    skinTone.lineBreakMode=NSLineBreakByWordWrapping;
    skinTone.numberOfLines=6;
    skinTone.textAlignment=NSTextAlignmentCenter;
    skinTone.backgroundColor=[UIColor clearColor];
    
    
    if([[NSUserDefaults standardUserDefaults]valueForKey:@"username"])
    {
        [skinTone setText:[[NSUserDefaults standardUserDefaults]valueForKey:@"username"]];
    }
    else
    [skinTone setText:@""];

    [v addSubview:skinTone];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    backButton.frame = CGRectMake(menuV.frame.size.width-100, 10, 30, 30);
    [backButton setTitleColor:[UIColor colorWithRed:133/255.0 green:133/255.0 blue:133/255.0 alpha:1.0] forState:UIControlStateNormal];
    [backButton setTitle:@"" forState:UIControlStateNormal];
    UIImage *img = [[UIImage imageNamed:@"cross"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    [backButton setImage:img forState:UIControlStateNormal];
    backButton.layer.borderColor=[UIColor colorWithRed:133/255.0 green:133/255.0 blue:133/255.0 alpha:1.0].CGColor;
    //backButton.layer.borderWidth=1.0;
    //backButton.layer.cornerRadius=backButton.frame.size.width/2;
    backButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
    [backButton addTarget:self action:@selector(back_Action) forControlEvents:UIControlEventTouchUpInside];
    [menuV addSubview:backButton];
}
-(void)back_Action
{
    [menuV removeFromSuperview];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return menuArray.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CELL"];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }

    
    
    UIButton *changePass = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [changePass setTitle:[menuArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
//    changePass.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    changePass.frame = CGRectMake(20, 10,tableView.frame.size.width-40, 20);
    changePass.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:13];
    changePass.userInteractionEnabled=false;
    UIImage *img2 = [[UIImage imageNamed:menuImageArray[indexPath.row]]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [changePass setImage:img2 forState:UIControlStateNormal];
    [changePass setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cell addSubview:changePass];
    
    
//    UIFont * myFont =[UIFont fontWithName:@"Lato-Regular" size:12];
//    CGRect labelFrame1 = CGRectMake (100, 5,tableView.frame.size.width-80, 30);
//    UILabel *label1 = [[UILabel alloc] initWithFrame:labelFrame1];
//    [label1 setFont:myFont];
//    label1.textColor=[UIColor whiteColor];
//    label1.textAlignment=NSTextAlignmentLeft;
//    label1.backgroundColor=[UIColor clearColor];
//    [label1 setText:[menuArray objectAtIndex:indexPath.row]];
//    [cell addSubview:label1];
//
//    UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(77, 12, 16, 16)];
//    img.image=[UIImage imageNamed:menuImageArray[indexPath.row]];
//    [cell addSubview:img];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(50, 39, tableView.frame.size.width-100,0.3)];
    [view setBackgroundColor:[UIColor colorWithRed:255.0 green:255.0 blue:255.0 alpha:0.1]];
    [cell addSubview:view];
    
    cell.backgroundColor=[UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   if(indexPath.row==0)
   {
       self.tabBarController.selectedIndex=2;
       [self back_Action];
   }
    if(indexPath.row==7)
    {
   
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:@"" forKey:@"userid"];
        [userDefaults setObject:@"" forKey:@"username"];
        [userDefaults setObject:nil forKey:@"color"];
        [userDefaults synchronize];
        self.tabBarController.selectedIndex=2;
        [self back_Action];
    }
    if(indexPath.row==4)
    {
        self.tabBarController.selectedIndex=0;
        [self back_Action];
    }
    if(indexPath.row==5)
    {
        aboutUsViewController *controller = [[aboutUsViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
        [self back_Action];
    }
    if(indexPath.row==6)
    {
        contactUsViewController *controller = [[contactUsViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
        [self back_Action];
    }
//    if(indexPath.row==7)
//    {
//        videoViewController *controller = [[videoViewController alloc]init];
//        [self.navigationController pushViewController:controller animated:YES];
//        [self back_Action];
//    }
    if(indexPath.row==2)
    {
        historyViewController *controller = [[historyViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
        [self back_Action];
    }
    if(indexPath.row==1)
    {
        myOrderViewController *controller = [[myOrderViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
        [self back_Action];
    }
    if(indexPath.row==3)
    {
        notificationsViewController *controller = [[notificationsViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
        [self back_Action];
    }
    
    
    
}

@end
