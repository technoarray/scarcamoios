//
//  chooseViewController.m
//  ScarCamo
//
//  Created by mac on 20/02/18.
//  Copyright © 2018 RohitMahajan. All rights reserved.
//

#import "chooseViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "AFNetworking.h"
#import "ViewController.h"
@interface chooseViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    NSMutableArray *colorCode;
}
@property (nonatomic, strong) IBOutlet iCarousel *carousel;
@property (retain) AVCaptureVideoPreviewLayer *PreviewLayer;

@end

@implementation chooseViewController
- (void)dealloc
{

    _carousel.delegate = nil;
    _carousel.dataSource = nil;
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    
    //free up memory by releasing subviews
    self.carousel = nil;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [self.tabBarController.tabBar setHidden:false];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];

   
    [self.tabBarController.tabBar setHidden:true];

   
    
    self.navigationController.navigationBarHidden=YES;
    
    UIView *v=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, (self.view.frame.size.height/3)*3)];
    
    [self.view addSubview:v];
    
    
    AVCaptureSession *CaptureSession = [[AVCaptureSession alloc] init];
    
    AVCaptureDevice *VideoDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if (VideoDevice)
    {
        NSError *error;
        AVCaptureDeviceInput *VideoInputDevice;
        VideoInputDevice = [[AVCaptureDeviceInput alloc] initWithDevice:[self CameraWithPosition:AVCaptureDevicePositionFront] error:&error];

        if (!error)
        {
            if ([CaptureSession canAddInput:VideoInputDevice])
                [CaptureSession addInput:VideoInputDevice];
            else
                NSLog(@"Couldn't add video input");
        }
        else
        {
            NSLog(@"Couldn't create video input");
        }
    }
    else
    {
        NSLog(@"Couldn't create video capture device");
    }

    NSLog(@"Adding video preview layer");
    _PreviewLayer=[[AVCaptureVideoPreviewLayer alloc]initWithSession:CaptureSession];
    [[self PreviewLayer] setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    NSLog(@"Setting image quality");
    [CaptureSession setSessionPreset:AVCaptureSessionPresetHigh];
    if ([CaptureSession canSetSessionPreset:AVCaptureSessionPreset352x288])
        [CaptureSession setSessionPreset:AVCaptureSessionPreset352x288];
    
    
    
    //----- DISPLAY THE PREVIEW LAYER -----
    //Display it full screen under out view controller existing controls
    NSLog(@"Display the preview layer");
    CGRect layerRect = [[v layer] bounds];
    [_PreviewLayer setBounds:layerRect];
    [_PreviewLayer setPosition:CGPointMake(CGRectGetMidX(layerRect),
                                          CGRectGetMidY(layerRect))];
    
    UIView *CameraView = [[UIView alloc] init] ;
    [v addSubview:CameraView];
    [v sendSubviewToBack:CameraView];
    
    [[CameraView layer] addSublayer:_PreviewLayer];
    
    
    //----- START THE CAPTURE SESSION RUNNING -----
    [CaptureSession startRunning];
    
    
    
    UIView *downView=[[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-self.view.frame.size.height/3+20, self.view.frame.size.width, self.view.frame.size.height/3-20)];
    downView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:downView];
    
    
    UIButton *next = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    next.tag = 102;
    next.frame = CGRectMake(self.view.frame.size.width/2-50,downView.frame.size.height-50, 100, 40);
    [next setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [next setTitle:@"NEXT" forState:UIControlStateNormal];
    [next setBackgroundImage:[UIImage imageNamed:@"btn"] forState:UIControlStateNormal];
    next.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:16];
    [next addTarget:self action:@selector(next_Action:) forControlEvents:UIControlEventTouchUpInside];
    [downView addSubview:next];
    
    UIImageView *arrow_img=[[UIImageView alloc]initWithFrame:CGRectMake(next.frame.size.width-30, next.frame.size.height/2-10, 20, 20)];
    arrow_img.image=[UIImage imageNamed:@"arrBlack"];
    [next addSubview:arrow_img];
    
    
    
    
    UIScrollView *scrv=[[UIScrollView alloc]initWithFrame:CGRectMake(0, downView.frame.origin.y-50, downView.frame.size.width, downView.frame.size.height)];
    scrv.backgroundColor=[UIColor clearColor];
    [self.view addSubview:scrv];
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:@"http://18.219.134.84/slim_api/public/shades/all" parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        if([[responseObject valueForKey:@"code"]intValue]==1)
        {
            
            colorCode=[[responseObject valueForKey:@"result"]mutableCopy];
            //int x=10;
            _carousel=[[iCarousel alloc]initWithFrame:CGRectMake(0, 0, scrv.frame.size.width,  scrv.frame.size.height)];
            _carousel.backgroundColor=[UIColor clearColor];
            _carousel.delegate=self;
            _carousel.dataSource=self;
            _carousel.type = iCarouselTypeRotary;
            
            _carousel.pagingEnabled=YES;
            [scrv addSubview:_carousel];
 
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                                message:[responseObject valueForKey:@"message"]
                                                               delegate:nil
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil];
            [alertView show];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    

    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    backButton.frame = CGRectMake(self.view.frame.size.width-50, 30, 40, 40);
    [backButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [backButton setTitle:@"" forState:UIControlStateNormal];
    [backButton setImage:[UIImage imageNamed:@"cross"] forState:UIControlStateNormal];
    backButton.layer.borderColor=[UIColor blackColor].CGColor;
    backButton.layer.borderWidth=1.0;
    backButton.layer.cornerRadius=backButton.frame.size.width/2;
    backButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
    [backButton addTarget:self action:@selector(back_Action) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    
}
-(void)back_Action
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (AVCaptureDevice *) CameraWithPosition:(AVCaptureDevicePosition) Position
{
    AVCaptureDeviceDiscoverySession *captureDeviceDiscoverySession = [AVCaptureDeviceDiscoverySession discoverySessionWithDeviceTypes:@[AVCaptureDeviceTypeBuiltInWideAngleCamera]mediaType:AVMediaTypeVideo position:Position];
    NSArray *Devices = [captureDeviceDiscoverySession devices];
    for (AVCaptureDevice *Device in Devices)
    {
        if ([Device position] == Position)
        {
            return Device;
        }
    }
    return nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)next_Action:(UIButton*)btn
{
    
    
    NSArray *items = [[[colorCode objectAtIndex:_carousel.currentItemView.tag]valueForKey:@"color_code"] componentsSeparatedByString:@","];

    UIColor *color=[UIColor colorWithRed:[items[0]intValue]/255.0 green:[items[1]intValue]/255.0 blue:[items[2]intValue]/255.0 alpha:1.0];
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:color];
    [[NSUserDefaults standardUserDefaults]setValue:data forKey:@"color"];
    [[NSUserDefaults standardUserDefaults]setValue:[[colorCode objectAtIndex:_carousel.currentItemView.tag]valueForKey:@"color_code"] forKey:@"color_code"];
    [[NSUserDefaults standardUserDefaults]setValue:[[colorCode objectAtIndex:_carousel.currentItemView.tag]valueForKey:@"id"] forKey:@"index"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
   // NSString *userid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];

    NSMutableDictionary *dict= [[[NSUserDefaults standardUserDefaults]valueForKey:@"historyy"]mutableCopy];
    if(dict==nil)
    {
        dict=[NSMutableDictionary new];
    }
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"dd.MM.yyyy HH:mm:ss"]; // Date formater
    NSString *date = [dateformate stringFromDate:[NSDate date]];
    
    NSArray *arr=[NSArray arrayWithObjects:[[colorCode objectAtIndex:_carousel.currentItemView.tag]valueForKey:@"color_code"],[[colorCode objectAtIndex:_carousel.currentItemView.tag]valueForKey:@"id"], nil];
    
    
    [dict setValue:arr forKey:date];
    
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:dict forKey:@"historyy"];
    [userDefaults synchronize];
    
    
    
    
    
    
    
    
    
        
    self.tabBarController.selectedIndex=0;
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}
- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //return the total number of items in the carousel
    return [colorCode count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    
    if (view == nil)
    {
        
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, 200.0f, 120)];
        view.contentMode = UIViewContentModeCenter;
        NSArray *items = [[[colorCode objectAtIndex:index]valueForKey:@"color_code"] componentsSeparatedByString:@","];
        view.tag=index;
        view.backgroundColor=[UIColor colorWithRed:[items[0]intValue]/255.0 green:[items[1]intValue]/255.0 blue:[items[2]intValue]/255.0 alpha:1.0];
    }
    else
    {
        NSArray *items = [[[colorCode objectAtIndex:index]valueForKey:@"color_code"] componentsSeparatedByString:@","];
        view.tag=index;
        view.backgroundColor=[UIColor colorWithRed:[items[0]intValue]/255.0 green:[items[1]intValue]/255.0 blue:[items[2]intValue]/255.0 alpha:1.0];
    }
    
    
    return view;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    if (option == iCarouselOptionSpacing)
    {
        return value * 1.7;
    }
    return value;
}
- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel;

{
    NSLog(@"%d",(int)carousel.currentItemView.tag);
}

@end
