//
//  chooseViewController.h
//  ScarCamo
//
//  Created by mac on 20/02/18.
//  Copyright © 2018 RohitMahajan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"

@interface chooseViewController : UIViewController<iCarouselDataSource, iCarouselDelegate>

@end
