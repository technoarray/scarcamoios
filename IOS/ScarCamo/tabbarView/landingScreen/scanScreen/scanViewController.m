//
//  scanViewController.m
//  ScarCamo
//
//  Created by mac on 25/02/18.
//  Copyright © 2018 RohitMahajan. All rights reserved.
//

#import "scanViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "showScanedViewController.h"
#import "AFNetworking.h"


@interface scanViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    UIImagePickerController *imagePickerController;
    UIView *v;
    AVCaptureSession *CaptureSession;
    UIImage *currentImage;
    AVCaptureDevice *VideoDevice;
    UIView *downView;
        NSMutableArray *colorCode,*colorId;
    UILabel *lbl1,*lbl2,*lbl3;
    int colorCount;
    
    NSTimer *colorTimer;
}
@property (retain) AVCaptureVideoPreviewLayer *PreviewLayer;
@property (nonatomic, strong) IBOutlet iCarousel *carousel;

@end

@implementation scanViewController
-(void)viewWillDisappear:(BOOL)animated
{
    [colorTimer invalidate];
    [self.tabBarController.tabBar setHidden:false];
    [downView removeFromSuperview];
}
-(void)viewWillAppear:(BOOL)animated
{
    lbl1.backgroundColor=[UIColor clearColor];
    lbl3.backgroundColor=[UIColor clearColor];
    lbl2.backgroundColor=[UIColor clearColor];

    colorCount=0;
    [CaptureSession startRunning];
    [self.tabBarController.tabBar setHidden:true];
    self.navigationController.navigationBarHidden=true;
    
//    [VideoDevice lockForConfiguration:nil];
//    [VideoDevice setTorchMode:AVCaptureTorchModeOn];
//    [VideoDevice setFlashMode:AVCaptureFlashModeOn];
//    [VideoDevice unlockForConfiguration];

    downView=[[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-80, self.view.frame.size.width, 100)];
    downView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:downView];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:@"http://18.219.134.84/slim_api/public/shades/all" parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        if([[responseObject valueForKey:@"code"]intValue]==1)
        {
            
            colorCode=[[responseObject valueForKey:@"result"]mutableCopy];
            
            //int x=10;
            _carousel=[[iCarousel alloc]initWithFrame:CGRectMake(0, 0, downView.frame.size.width,  downView.frame.size.height-10)];
            _carousel.backgroundColor=[UIColor clearColor];
            _carousel.delegate=self;
            _carousel.dataSource=self;
            _carousel.type = iCarouselTypeRotary;
            
            _carousel.pagingEnabled=YES;
            [downView addSubview:_carousel];
            
            [self click_action];
            
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                                message:[responseObject valueForKey:@"message"]
                                                               delegate:nil
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil];
            [alertView show];
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];


    self.view.backgroundColor=[UIColor whiteColor];
    
    v=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-100)];
    [self.view addSubview:v];
    
    CaptureSession = [[AVCaptureSession alloc] init];
    VideoDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    if (VideoDevice)
    {
        NSError *error;
        AVCaptureDeviceInput *VideoInputDevice;
        VideoInputDevice = [[AVCaptureDeviceInput alloc] initWithDevice:[self CameraWithPosition:AVCaptureDevicePositionBack] error:&error];
        
        if (!error)
        {
            if ([CaptureSession canAddInput:VideoInputDevice])
            {
               
                [CaptureSession addInput:VideoInputDevice];
            }
            else
                NSLog(@"Couldn't add video input");
        }
        else
        {
            NSLog(@"Couldn't create video input");
        }
    }
    else
    {
        NSLog(@"Couldn't create video capture device");
    }
    AVCaptureVideoDataOutput *output = [[AVCaptureVideoDataOutput alloc] init];
    [CaptureSession addOutput:output];
    NSLog(@"connections: %@", output.connections);
    
    // Configure your output.
    dispatch_queue_t queue = dispatch_queue_create("myQueue", NULL);
    [output setSampleBufferDelegate:self queue:queue];
    output.videoSettings = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA] forKey:(id)kCVPixelBufferPixelFormatTypeKey];

    NSLog(@"Adding video preview layer");
    _PreviewLayer=[[AVCaptureVideoPreviewLayer alloc]initWithSession:CaptureSession];
    [[self PreviewLayer] setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    NSLog(@"Setting image quality");
    [CaptureSession setSessionPreset:AVCaptureSessionPresetHigh];
    if ([CaptureSession canSetSessionPreset:AVCaptureSessionPresetHigh])
        [CaptureSession setSessionPreset:AVCaptureSessionPresetHigh];
    
    
    
    //----- DISPLAY THE PREVIEW LAYER -----
    //Display it full screen under out view controller existing controls
    NSLog(@"Display the preview layer");
    CGRect layerRect = [[v layer] bounds];
    [_PreviewLayer setBounds:layerRect];
    [_PreviewLayer setPosition:CGPointMake(CGRectGetMidX(layerRect),
                                           CGRectGetMidY(layerRect))];
    
    //UIView *CameraView = [[UIView alloc] init] ;
   // [v addSubview:CameraView];
   // [v sendSubviewToBack:CameraView];
    
    [[v layer] addSublayer:_PreviewLayer];
    
    
    //----- START THE CAPTURE SESSION RUNNING -----
    
    
    lbl1=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50, 30, 30, 30)];
    lbl1.text=@"1";
    lbl1.textAlignment=NSTextAlignmentCenter;
    lbl1.layer.cornerRadius=15.0;
    lbl1.textColor=[UIColor blackColor];
    lbl1.layer.borderColor=[UIColor greenColor].CGColor;
    lbl1.layer.borderWidth=1.0;
    lbl1.backgroundColor=[UIColor clearColor];
    lbl1.clipsToBounds=YES;
    [self.view addSubview:lbl1];
    
    lbl2=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-10, 30, 30, 30)];
    lbl2.text=@"2";
    lbl2.textColor=[UIColor blackColor];
    lbl2.textAlignment=NSTextAlignmentCenter;

    lbl2.layer.cornerRadius=15.0;
    lbl2.layer.borderColor=[UIColor greenColor].CGColor;
    lbl2.layer.borderWidth=1.0;
    lbl2.backgroundColor=[UIColor clearColor];
    lbl2.clipsToBounds=YES;
    [self.view addSubview:lbl2];
    
    lbl3=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2+30, 30, 30, 30)];
    lbl3.text=@"3";
    lbl3.textColor=[UIColor blackColor];
    lbl3.textAlignment=NSTextAlignmentCenter;
    lbl3.layer.cornerRadius=15.0;
    lbl3.layer.borderColor=[UIColor greenColor].CGColor;
    lbl3.layer.borderWidth=1.0;
    lbl3.backgroundColor=[UIColor clearColor];
    lbl3.clipsToBounds=YES;
    [self.view addSubview:lbl3];
    
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    backButton.frame = CGRectMake(self.view.frame.size.width-50, 30, 40, 40);
    [backButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [backButton setTitle:@"" forState:UIControlStateNormal];
    [backButton setImage:[UIImage imageNamed:@"cross"] forState:UIControlStateNormal];
    backButton.layer.borderColor=[UIColor blackColor].CGColor;
    backButton.layer.borderWidth=1.0;
    backButton.layer.cornerRadius=backButton.frame.size.width/2;
    backButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
    [backButton addTarget:self action:@selector(back_Action) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    
   
   
    
}
-(void)back_Action
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (AVCaptureDevice *) CameraWithPosition:(AVCaptureDevicePosition) Position
{
    AVCaptureDeviceDiscoverySession *captureDeviceDiscoverySession = [AVCaptureDeviceDiscoverySession discoverySessionWithDeviceTypes:@[AVCaptureDeviceTypeBuiltInWideAngleCamera]mediaType:AVMediaTypeVideo position:Position];
    NSArray *Devices = [captureDeviceDiscoverySession devices];
    for (AVCaptureDevice *Device in Devices)
    {
        if ([Device position] == Position)
        {
            return Device;
        }
    }
    return nil;
}
-(void)click_action
     {
         [NSTimer scheduledTimerWithTimeInterval:3.0f target:self
                                        selector:@selector(timerFired:) userInfo:nil repeats:NO];
     
        colorTimer= [NSTimer scheduledTimerWithTimeInterval:1.0f target:self
                                        selector:@selector(timerFiredforColor:) userInfo:nil repeats:YES];
         
     }
-(void)timerFiredforColor:(NSTimer *)timer
    {
        colorCount++;
        if(colorCount==1)
        {
            lbl1.backgroundColor=[UIColor whiteColor];
        }
        else  if(colorCount==2)
        {
            lbl2.backgroundColor=[UIColor whiteColor];
        }
        else  if(colorCount==3)
        {
            lbl3.backgroundColor=[UIColor whiteColor];
            [colorTimer invalidate];
        }
        
            
            
        
    }
-(void)timerFired:(NSTimer *)timer{
    
    [self getRGBAFromImage:currentImage atx:self.view.frame.size.width/2 atY:self.view.frame.size.height/2];
    [CaptureSession stopRunning];
    
    

}
-(void)timerFired1:(NSTimer *)timer{
    showScanedViewController *controller = [[showScanedViewController alloc]init];
    [self.navigationController pushViewController:controller animated:YES];
}
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer
       fromConnection:(AVCaptureConnection *)connection
{
    currentImage = [self imageFromSampleBuffer:sampleBuffer];
}

// Create a UIImage from sample buffer data
- (UIImage *) imageFromSampleBuffer:(CMSampleBufferRef) sampleBuffer
{
    //NSLog(@"imageFromSampleBuffer: called");
    // Get a CMSampleBuffer's Core Video image buffer for the media data
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    // Lock the base address of the pixel buffer
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    
    // Get the number of bytes per row for the pixel buffer
    void *baseAddress = CVPixelBufferGetBaseAddress(imageBuffer);
    
    // Get the number of bytes per row for the pixel buffer
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    // Get the pixel buffer width and height
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    
    // Create a device-dependent RGB color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // Create a bitmap graphics context with the sample buffer data
    CGContextRef context = CGBitmapContextCreate(baseAddress, width, height, 8, bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    // Create a Quartz image from the pixel data in the bitmap graphics context
    CGImageRef quartzImage = CGBitmapContextCreateImage(context);
    // Unlock the pixel buffer
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    
    
    // Free up the context and color space
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    // Create an image object from the Quartz image
    UIImage *image = [UIImage imageWithCGImage:quartzImage];
    
    // Release the Quartz image
    CGImageRelease(quartzImage);
    
    return (image);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSArray*)getRGBAFromImage:(UIImage*)image atx:(int)xp atY:(int)yp

{
    
    CGImageRef imageRef = [image CGImage];

    NSUInteger width = CGImageGetWidth(imageRef);

    NSUInteger height = CGImageGetHeight(imageRef);

    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();

    unsigned char *rawData = (unsigned char*) calloc(height * width * 4, sizeof(unsigned char));

    NSUInteger bytesPerPixel = 4;

    NSUInteger bytesPerRow = bytesPerPixel * width;

    NSUInteger bitsPerComponent = 8;

    CGContextRef context = CGBitmapContextCreate(rawData, width, height,

                                                 bitsPerComponent, bytesPerRow, colorSpace,

                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);

    CGColorSpaceRelease(colorSpace);



    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);

    CGContextRelease(context);

    // Now your rawData contains the image data in the RGBA8888 pixel format.

    int byteIndex = (bytesPerRow * yp) + xp * bytesPerPixel;

    CGFloat red   = (rawData[byteIndex] * 1.0) /255.0;

    CGFloat green = (rawData[byteIndex + 1] * 1.0)/255.0 ;

    CGFloat blue  = (rawData[byteIndex + 2] * 1.0)/255.0 ;

    CGFloat alpha = (rawData[byteIndex + 3] * 1.0) /255.0;

    byteIndex += 4;


    UIColor *color = [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
    

    
    
    UIView *v=[[UIView alloc]initWithFrame:self.view.frame];
    v.backgroundColor=color;
   // [self.view addSubview:v];

    NSDictionary *params = @{@"rgb"       : [NSString stringWithFormat:@"%f,%f,%f",red*255.0,green*255.0,blue*255.0],
                             };
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [manager POST:@"http://18.219.134.84/slim_api/public/shades/find_shade" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"JSON: %@", responseObject);
        if([[responseObject valueForKey:@"code"]intValue]==1)
        {
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:color];
            [[NSUserDefaults standardUserDefaults]setValue:data forKey:@"color"];
            [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%f",red] forKey:@"red"];
            [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%f",green] forKey:@"green"];
            [[NSUserDefaults standardUserDefaults]setValue:[NSString stringWithFormat:@"%f",blue] forKey:@"blue"];
            [[NSUserDefaults standardUserDefaults]setValue:[responseObject valueForKey:@"similar"] forKey:@"similar"];
            [[NSUserDefaults standardUserDefaults]setValue:[responseObject valueForKey:@"color_code"] forKey:@"color_code"];

           // NSMutableDictionary *dict
            
            
            int x=[[responseObject valueForKey:@"index"]intValue];
            [[NSUserDefaults standardUserDefaults]setValue:[[colorCode objectAtIndex:x]valueForKey:@"id"] forKey:@"index"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [_carousel scrollToItemAtIndex:[[responseObject valueForKey:@"index"]intValue] duration:2.0f];
            [NSTimer scheduledTimerWithTimeInterval:4.0f target:self
                                           selector:@selector(timerFired1:) userInfo:nil repeats:NO];
        }
        else
        {
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
    }];
    
    
    
    
    free(rawData);
    
    return 0;
    
}
- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    
    if (view == nil)
    {
        
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, 200.0f, 120)];
        view.contentMode = UIViewContentModeCenter;
        NSArray *items = [[[colorCode objectAtIndex:index]valueForKey:@"color_code"] componentsSeparatedByString:@","];
        view.tag=index;
        view.backgroundColor=[UIColor colorWithRed:[items[0]intValue]/255.0 green:[items[1]intValue]/255.0 blue:[items[2]intValue]/255.0 alpha:1.0];
    }
    else
    {
        NSArray *items = [[[colorCode objectAtIndex:index]valueForKey:@"color_code"] componentsSeparatedByString:@","];
        view.tag=index;
        view.backgroundColor=[UIColor colorWithRed:[items[0]intValue]/255.0 green:[items[1]intValue]/255.0 blue:[items[2]intValue]/255.0 alpha:1.0];
    }
    
    
    return view;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    if (option == iCarouselOptionSpacing)
    {
        return value * 1.7;
    }
    return value;
}
- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel;

{
    NSLog(@"%d",(int)carousel.currentItemView.tag);
}
- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //return the total number of items in the carousel
    return [colorCode count];
}

@end
