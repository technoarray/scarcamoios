//
//  showScanedViewController.m
//  ScarCamo
//
//  Created by mac on 03/03/18.
//  Copyright © 2018 RohitMahajan. All rights reserved.
//

#import "showScanedViewController.h"
#import "ViewController.h"
@interface showScanedViewController ()
{
    NSString *color_code;
}
@end

@implementation showScanedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //NSData *data =[[NSUserDefaults standardUserDefaults]valueForKey:@"color"];
    //UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    color_code=[[NSUserDefaults standardUserDefaults]valueForKey:@"color_code"];
    
    CGFloat red =[[[NSUserDefaults standardUserDefaults]valueForKey:@"red"] floatValue];
    CGFloat green =[[[NSUserDefaults standardUserDefaults]valueForKey:@"green"]floatValue];
    CGFloat blue =[[[NSUserDefaults standardUserDefaults]valueForKey:@"blue"]floatValue];

    self.view.backgroundColor=[UIColor colorWithRed:red green:green blue:blue alpha:1.0];
   
    
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(25, 54, self.view.frame.size.width-50, 30)];
    lbl.textColor=[UIColor whiteColor];
    lbl.numberOfLines=2;
    lbl.textAlignment=NSTextAlignmentCenter;
    if(self.view.frame.size.width==320)
        lbl.font=[UIFont fontWithName:@"Helvetica" size:20];
     else
    lbl.font=[UIFont fontWithName:@"Helvetica" size:22];
    lbl.text=@"THIS IS YOUR SKIN TONE";
    [self.view addSubview:lbl];
    
    
    UILabel *lbl1 = [[UILabel alloc]initWithFrame:CGRectMake(25, 80, self.view.frame.size.width-50, 70)];
    lbl1.textColor=[UIColor whiteColor];
    lbl1.numberOfLines=2;
    lbl1.textAlignment=NSTextAlignmentCenter;
    lbl1.font=[UIFont fontWithName:@"Helvetica" size:14];
    lbl1.text=@"You can continue with this or you can scan again.";
    [self.view addSubview:lbl1];
    
    
    UIButton *next = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    next.tag = 102;
    next.frame = CGRectMake(self.view.frame.size.width/2-150,self.view.frame.size.height/2+100, 140, 40);
    [next setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [next setTitle:@"Continue" forState:UIControlStateNormal];
    [next setBackgroundImage:[UIImage imageNamed:@"btn"] forState:UIControlStateNormal];
    next.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
    [next addTarget:self action:@selector(click_action) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:next];
    
    UIImageView *arrow_img=[[UIImageView alloc]initWithFrame:CGRectMake(next.frame.size.width-30, next.frame.size.height/2-10, 20, 20)];
    arrow_img.image=[UIImage imageNamed:@"arrBlack"];
   // [next addSubview:arrow_img];
    
    
    UIButton *scanAgain = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    scanAgain.tag = 102;
    scanAgain.frame = CGRectMake(self.view.frame.size.width/2+10,self.view.frame.size.height/2+100, 140, 40);
    [scanAgain setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [scanAgain setTitle:@"Scan Again" forState:UIControlStateNormal];
    [scanAgain setBackgroundImage:[UIImage imageNamed:@"btn"] forState:UIControlStateNormal];
    scanAgain.titleLabel.font =  [UIFont fontWithName:@"Helvetica-Bold" size:14];
;
    [scanAgain addTarget:self action:@selector(scan_action) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:scanAgain];
    

    
    UIView *v=[[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-100, self.view.frame.size.height/2-80, 200, 100)];
    NSArray *items = [color_code componentsSeparatedByString:@","];
    v.backgroundColor=[UIColor colorWithRed:[items[0]intValue]/255.0 green:[items[1]intValue]/255.0 blue:[items[2]intValue]/255.0 alpha:1.0];
    v.layer.cornerRadius=10;
    v.layer.borderWidth=1.0;
    v.layer.borderColor=[UIColor greenColor].CGColor;
    [self.view addSubview:v];
    
    UILabel *lbll=[[UILabel alloc]initWithFrame:CGRectMake(10, v.frame.size.height/2-20, 180, 40)];
    lbll.text=[NSString stringWithFormat:@"Your Skin Tone Matched %@%@ with this color",[[NSUserDefaults standardUserDefaults]valueForKey:@"similar"],@"%"];
    lbll.textColor=[UIColor whiteColor];
    lbll.numberOfLines=2;
    lbll.textAlignment=NSTextAlignmentCenter;
    
    lbll.font=[UIFont fontWithName:@"Helvetica-Bold" size:12];

    [v addSubview:lbll];
    
    
    
    // Do any additional setup after loading the view.
}
-(void)click_action
{
    NSMutableDictionary *dict= [[[NSUserDefaults standardUserDefaults]valueForKey:@"historyy"]mutableCopy];
    if(dict==nil)
    {
        dict=[NSMutableDictionary new];
    }
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"dd.MM.yyyy HH:mm:ss"]; // Date formater
    NSString *date = [dateformate stringFromDate:[NSDate date]];
    
    NSArray *arr=[NSArray arrayWithObjects:color_code,[[NSUserDefaults standardUserDefaults]valueForKey:@"index"], nil];
    
    
    [dict setValue:arr forKey:date];
    
    
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:dict forKey:@"historyy"];
            [userDefaults synchronize];
    
    
//    NSString *userid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
//    if(userid == (id)[NSNull null]|| userid.length == 0  || [userid isEqual:@"(null)"])
//    {
//        ViewController *controller = [[ViewController alloc]init];
//        [self.navigationController pushViewController:controller animated:NO];
//    }
//    else
//    {
//        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//        [userDefaults setObject:@"no" forKey:@"product"];
//        [userDefaults synchronize];
    self.tabBarController.selectedIndex=0;
    [self.navigationController popToRootViewControllerAnimated:YES];
//    }
}
-(void)scan_action
{
    [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:@"color"];
    [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:@"similar"];
    [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:@"c"];
        [[NSUserDefaults standardUserDefaults]setValue:@"" forKey:@"index"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
