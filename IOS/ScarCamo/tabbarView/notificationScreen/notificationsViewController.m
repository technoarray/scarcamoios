//
//  notificationsViewController.m
//  ScarCamo
//
//  Created by mac on 28/02/18.
//  Copyright © 2018 RohitMahajan. All rights reserved.
//

#import "notificationsViewController.h"
#import "AsyncImageView.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
@interface notificationsViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *tablev;
    NSMutableArray *notiArray;
    MBProgressHUD *HUD;
}
@end

@implementation notificationsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView *navView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:navView];
    
    
    UIImageView *logo=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-68.5, 32, 137, 23)];
    logo.image=[UIImage imageNamed:@"logo1"];
    logo.contentMode = UIViewContentModeScaleAspectFit;
    [navView addSubview:logo];
    
    UIView *v=[[UIView alloc]initWithFrame:CGRectMake(0, 63, self.view.frame.size.width, 1)];
    v.backgroundColor=[UIColor lightGrayColor];
    v.alpha=0.5;
    [navView addSubview:v];
    
    
    
    UIImage *img = [[UIImage imageNamed:@"back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    menuButton.frame = CGRectMake(10,25, 40, 40);
    [menuButton setImage:img forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back_action:) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:menuButton];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    UILabel *nameLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 80, self.view.frame.size.width, 30)];
    nameLbl.text=@"NOTIFICATIONS";
    nameLbl.font = [UIFont fontWithName:@"Helvetica" size:18];
    nameLbl.textColor=[UIColor blackColor];
    nameLbl.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:nameLbl];
    
    UILabel *history=[[UILabel alloc]initWithFrame:CGRectMake(0, 130, self.view.frame.size.width, 30)];
    history.text=@"No Notification Found";
    history.font = [UIFont fontWithName:@"Helvetica" size:15];
    history.textColor=[UIColor blackColor];
    history.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:history];
    
    
    tablev = [[UITableView alloc] initWithFrame:CGRectMake(0, 120, self.view.frame.size.width, self.view.frame.size.height-64-40) style:UITableViewStylePlain];
    tablev.delegate = self;
    tablev.dataSource = self;
    tablev.backgroundColor = [UIColor clearColor];
    [self.view addSubview:tablev];
    
    tablev.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    HUD=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText=@"Please Wait";
    NSString *st=[NSString stringWithFormat:@"http://18.219.134.84/slim_api/public/notifications/show/%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:st parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@,    %@", responseObject,st);
        
        notiArray=[[responseObject valueForKey:@"result"]mutableCopy];
        [tablev reloadData];
        HUD.hidden=true;
        
        
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    
    // Do any additional setup after loading the view.
}
-(void)back_action:(UIButton*)btn
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)theTableView
{
    return 1;
}

// number of row in the section, I assume there is only 1 row
- (NSInteger)tableView:(UITableView *)theTableView numberOfRowsInSection:(NSInteger)section
{
    return notiArray.count;
}

// the cell will be returned to the tableView
- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [theTableView dequeueReusableCellWithIdentifier:@"CELL"];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    
    
    
    AsyncImageView* productImage=[[AsyncImageView alloc]initWithFrame:CGRectMake(5, 15, 20, 20)];
    productImage.contentMode = UIViewContentModeScaleAspectFit;
    productImage.backgroundColor=[UIColor clearColor];
    productImage.image=[UIImage imageNamed:@"bell"];
    [cell addSubview:productImage];
//
//    
    UILabel *nameLbl=[[UILabel alloc]initWithFrame:CGRectMake(30, 5, self.view.frame.size.width-70, 40)];
    nameLbl.numberOfLines=2;
    nameLbl.font = [UIFont fontWithName:@"Helvetica" size:16];
    nameLbl.textColor=[UIColor blackColor];
    nameLbl.textAlignment=NSTextAlignmentLeft;
    nameLbl.text=[notiArray[indexPath.row] valueForKey:@"message"];
    [cell addSubview:nameLbl];
    
    UIImage *img = [[UIImage imageNamed:@"close"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    UIButton *delBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    delBtn.tag=indexPath.row;
    delBtn.frame = CGRectMake(self.view.frame.size.width-40,15, 20, 20);
    [delBtn setImage:img forState:UIControlStateNormal];
    [delBtn addTarget:self action:@selector(deleteNoti:) forControlEvents:UIControlEventTouchUpInside];
    [cell addSubview:delBtn];
//
//    UILabel *priceLbl=[[UILabel alloc]initWithFrame:CGRectMake(40, 30, cell.frame.size.width-40, 20)];
//    priceLbl.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
//    priceLbl.textColor=[UIColor blackColor];
//    priceLbl.text=[NSString stringWithFormat:@"Total: $%@",[orderArray[indexPath.row] valueForKey:@"price"]];
//    priceLbl.textAlignment=NSTextAlignmentLeft;
//    [cell addSubview:priceLbl];
//    NSString *st;
//    if([[orderArray[indexPath.row] valueForKey:@"status"]intValue]==1)
//        st=@"processing";
//    else if([[orderArray[indexPath.row] valueForKey:@"status"]intValue]==1)
//        st=@"Dispatched";
//    else
//        st=@"Delivered";
//    NSArray *items = [[orderArray[indexPath.row] valueForKey:@"created_date"] componentsSeparatedByString:@" "];
//    
//    UILabel *timeLbl=[[UILabel alloc]initWithFrame:CGRectMake(40, 50, cell.frame.size.width-40, 20)];
//    timeLbl.font = [UIFont fontWithName:@"Helvetica" size:14];
//    timeLbl.textColor=[UIColor lightGrayColor];
//    timeLbl.text=[NSString stringWithFormat:@"%@ - %@",items[0],st];
//    timeLbl.textAlignment=NSTextAlignmentLeft;
//    [cell addSubview:timeLbl];
//    
//    
//    UILabel *discLbl=[[UILabel alloc]initWithFrame:CGRectMake(40, 70, cell.frame.size.width-45, 40)];
//    discLbl.font = [UIFont fontWithName:@"Helvetica" size:15];
//    discLbl.textColor=[UIColor blackColor];
//    discLbl.text=[orderArray[indexPath.row] valueForKey:@"description"];
//    discLbl.numberOfLines=2;
//    discLbl.textAlignment=NSTextAlignmentLeft;
//    [cell addSubview:discLbl];
//
    UIView *v=[[UIView alloc]initWithFrame:CGRectMake(20, 49, self.view.frame.size.width-40, 1)];
    v.backgroundColor=[UIColor lightGrayColor];
    v.alpha=0.5;
    [cell addSubview:v];
    
    cell.backgroundColor=[UIColor whiteColor];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}
#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
-(void)deleteNoti:(UIButton*)btn
{
    HUD=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText=@"Please Wait";

    
    
    
    

    

    
    
    
    NSDictionary *params = @{
                             @"id"            : [notiArray[btn.tag] valueForKey:@"id"],
                             };
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        
    [manager POST:@"http://18.219.134.84/slim_api/public/notification/delete" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if([[responseObject valueForKey:@"code"]intValue]==1)
        {
            NSString *st=[NSString stringWithFormat:@"http://18.219.134.84/slim_api/public/notifications/show/%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"userid"]];
            AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
            [manager GET:st parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
                NSLog(@"JSON: %@,    %@", responseObject,st);
                
                notiArray=[[responseObject valueForKey:@"result"]mutableCopy];
                [tablev reloadData];
                HUD.hidden=true;
                
                
                
                
            } failure:^(NSURLSessionTask *operation, NSError *error) {
                NSLog(@"Error: %@", error);
                HUD.hidden=true;
            }];
          
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                                message:[responseObject valueForKey:@"message"]
                                                               delegate:nil
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil];
            [alertView show];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        HUD.hidden=true;

    }];
}
@end
