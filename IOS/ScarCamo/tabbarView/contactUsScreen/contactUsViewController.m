//
//  contactUsViewController.m
//  ScarCamo
//
//  Created by mac on 27/02/18.
//  Copyright © 2018 RohitMahajan. All rights reserved.
//

#import "contactUsViewController.h"

@interface contactUsViewController ()<UITextFieldDelegate,UITextViewDelegate,UIWebViewDelegate>

@end

@implementation contactUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIView *navView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:navView];
    
    
    UIImageView *logo=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-68.5, 32, 137, 23)];
    logo.image=[UIImage imageNamed:@"logo1"];
    logo.contentMode = UIViewContentModeScaleAspectFit;
    [navView addSubview:logo];
    
    UIView *v=[[UIView alloc]initWithFrame:CGRectMake(0, 63, self.view.frame.size.width, 1)];
    v.backgroundColor=[UIColor lightGrayColor];
    v.alpha=0.5;
    [navView addSubview:v];
    
    
    UIImage *img = [[UIImage imageNamed:@"back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    menuButton.frame = CGRectMake(10,25, 40, 40);
    [menuButton setImage:img forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back_action:) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:menuButton];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    UILabel *nameLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 80, self.view.frame.size.width, 30)];
    nameLbl.text=@"CONATCT US";
    nameLbl.font = [UIFont fontWithName:@"Helvetica" size:18];
    nameLbl.textColor=[UIColor blackColor];
    nameLbl.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:nameLbl];
    
    
    
    
    namefeild=[[UITextField alloc]initWithFrame:CGRectMake(30, 115, self.view.frame.size.width-60, 40)];
    namefeild.font = [UIFont fontWithName:@"Helvetica" size:15];
    namefeild.textColor=[UIColor blackColor];
    namefeild.textAlignment=NSTextAlignmentLeft;
    namefeild.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"  Name" attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
    namefeild.layer.borderColor=[UIColor lightGrayColor].CGColor;
    namefeild.layer.borderWidth=1.0;
    namefeild.delegate=self;
    [self.view addSubview:namefeild];
    

    
    email=[[UITextField alloc]initWithFrame:CGRectMake(30, 165, self.view.frame.size.width-60, 40)];
    email.textColor=[UIColor blackColor];
    email.textAlignment=NSTextAlignmentLeft;
    email.font = [UIFont fontWithName:@"Helvetica" size:15];
    email.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"  Email" attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
    email.layer.borderColor=[UIColor lightGrayColor].CGColor;
    email.layer.borderWidth=1.0;
    email.delegate=self;
    [self.view addSubview:email];
    
    massage=[[UITextView alloc]initWithFrame:CGRectMake(30, 215, self.view.frame.size.width-60, 90)];
    massage.textColor=[UIColor blackColor];
    massage.textAlignment=NSTextAlignmentLeft;
    massage.font = [UIFont fontWithName:@"Helvetica" size:15];
    massage.text=@"Message";
    massage.layer.borderColor=[UIColor lightGrayColor].CGColor;
    massage.layer.borderWidth=1.0;
    massage.delegate=self;
    [self.view addSubview:massage];

    UIButton *sendButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [sendButton addTarget:self action:@selector(sendAction) forControlEvents:UIControlEventTouchUpInside];
    [sendButton setTitle:@"SEND" forState:UIControlStateNormal];
    [sendButton setBackgroundImage:[UIImage imageNamed:@"btn"] forState:UIControlStateNormal];
    sendButton.frame = CGRectMake(self.view.frame.size.width/2-50, self.view.frame.size.height-200, 100, 50);
    sendButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
    [sendButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.view addSubview:sendButton];
    
    NSURL *url;
  
        url=[NSURL URLWithString:@"http://18.219.134.84/pages/contactus.php"];
   
    
     UIWebView  *webV=[[UIWebView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width,self.view.frame.size.height-90)];
    
    [self.view addSubview:webV];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    webV.delegate = self;
    webV.scalesPageToFit = YES;
    [webV loadRequest:requestObj];
    
   

    
}
-(void)webViewDidFinishLoad:(UIWebView *)webView {
    //HUD.hidden=YES;
    NSLog(@"finish");
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    // HUD.hidden=YES;
    NSLog(@"Error for WEBVIEW: %@", [error description]);
}

-(void)sendAction
{
    
}
-(void)back_action:(UIButton*)btn
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    //    [scrv setContentOffset:CGPointMake(0, 0) animated:YES];
    [textField resignFirstResponder];
    return YES;
}
#pragma mark - Text view deligates
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@"\n"]) {
        
        if([textView.text isEqualToString:@""])
        {
            textView.text=@"Message";
        }
        
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if([textView.text isEqualToString:@"Message"])
    {
        textView.text=@"";
    }
    return YES;
}

@end
