//
//  videoViewController.m
//  ScarCamo
//
//  Created by mac on 27/02/18.
//  Copyright © 2018 RohitMahajan. All rights reserved.
//

#import "videoViewController.h"

@interface videoViewController ()

@end

@implementation videoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIView *navView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:navView];
    
    
    UIImageView *logo=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-68.5, 32, 137, 23)];
    logo.image=[UIImage imageNamed:@"logo1"];
    logo.contentMode = UIViewContentModeScaleAspectFit;
    [navView addSubview:logo];
    
    UIView *v=[[UIView alloc]initWithFrame:CGRectMake(0, 63, self.view.frame.size.width, 1)];
    v.backgroundColor=[UIColor lightGrayColor];
    v.alpha=0.5;
    [navView addSubview:v];
    
    
    UIImage *img = [[UIImage imageNamed:@"right-arrow"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    menuButton.frame = CGRectMake(10,25, 40, 40);
    [menuButton setImage:img forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back_action:) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:menuButton];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    UILabel *nameLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 80, self.view.frame.size.width, 30)];
    nameLbl.text=@"3D VIDEO";
    nameLbl.font = [UIFont fontWithName:@"Helvetica" size:18];
    nameLbl.textColor=[UIColor blackColor];
    nameLbl.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:nameLbl];
}
-(void)back_action:(UIButton*)btn
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
