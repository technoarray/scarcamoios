//
//  profileViewController.m
//  ScarCamo
//
//  Created by mac on 25/02/18.
//  Copyright © 2018 RohitMahajan. All rights reserved.
//

#import "profileViewController.h"
#import "ViewController.h"
#import "AFNetworking.h"
#import "AsyncImageView.h"
#import "changeAddressViewController.h"
#import "changePasswordViewController.h"
#import "MBProgressHUD.h"

#import "aboutUsViewController.h"
#import "contactUsViewController.h"
#import "historyViewController.h"
#import "myOrderViewController.h"
#import "notificationsViewController.h"
@interface profileViewController ()<UITextFieldDelegate>
{
    UILabel *addressLbl;
    UILabel *addressDetailLbl,*addressDetailLbl1,*addressDetailLbl2,*addressDetailLbl3;
    MBProgressHUD *HUD;
    UIView *menuV;
    UITableView *tableView;
    NSMutableArray *menuArray,*menuImageArray;
}
@end

@implementation profileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tabBarController.tabBar setHidden:false];
    self.navigationController.navigationBarHidden=true;
    self.view.backgroundColor=[UIColor whiteColor];
    UIView *navView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    
    UIImageView *logo=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-68.5, 32, 137, 23)];
    logo.image=[UIImage imageNamed:@"logo1"];
    logo.contentMode = UIViewContentModeScaleAspectFit;
    [navView addSubview:logo];
    
    UIView *v=[[UIView alloc]initWithFrame:CGRectMake(0, 63, self.view.frame.size.width, 1)];
    v.backgroundColor=[UIColor lightGrayColor];
    v.alpha=0.5;
    [navView addSubview:v];
    
    NSString *userid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    
    if(userid == (id)[NSNull null]|| userid.length == 0  || [userid isEqual:@"(null)"])
    {
        menuArray=[[NSMutableArray alloc]initWithObjects:@" MY PROFILE",@" MY ORDER",@" HISTORY",@" NOTIFICATIONS",@" PRODUCT",@" ABOUT US",@" CONTACT US", nil];
        menuImageArray=[[NSMutableArray alloc]initWithObjects:@"profile",@"mOrder",@"history",@"noti",@"prod",@"about",@"contactUs", nil];
    }
    else
    {
        menuArray=[[NSMutableArray alloc]initWithObjects:@" MY PROFILE",@" MY ORDER",@" HISTORY",@" NOTIFICATIONS",@" PRODUCT",@" ABOUT US",@" CONTACT US",@" LOGOUT", nil];
        menuImageArray=[[NSMutableArray alloc]initWithObjects:@"profile",@"mOrder",@"history",@"noti",@"prod",@"about",@"contactUs",@"logout", nil];
    }
    
    UIImage *img = [[UIImage imageNamed:@"menu"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    menuButton.frame = CGRectMake(10,25, 40, 40);
    [menuButton setImage:img forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(menu_action:) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:menuButton];
    
    
    [self.view addSubview:navView];
    
    
   
    
    username=[[UITextField alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-150, 65, 300, 40)];
    username.font = [UIFont fontWithName:@"Helvetica" size:22];
    username.textColor=[UIColor blackColor];
    username.textAlignment=NSTextAlignmentCenter;
   // username.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
    username.userInteractionEnabled=false;
    username.delegate=self;
    [self.view addSubview:username];
    

    UIView *sep=[[UIView alloc]initWithFrame:CGRectMake(20, 110, self.view.frame.size.width-40, 1)];
    sep.backgroundColor=[UIColor lightGrayColor];
    [self.view addSubview:sep];
    
    
    
    UILabel *emailLbl=[[UILabel alloc]initWithFrame:CGRectMake(20, 120, 200, 20)];
    emailLbl.text=@"Email";
    emailLbl.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
    emailLbl.textColor=[UIColor blackColor];
    emailLbl.textAlignment=NSTextAlignmentLeft;
    [self.view addSubview:emailLbl];
    
    
    email=[[UITextField alloc]initWithFrame:CGRectMake(20, 145, 200, 20)];
    email.textColor=[UIColor blackColor];
    email.userInteractionEnabled=false;
    email.textAlignment=NSTextAlignmentLeft;
    email.font = [UIFont fontWithName:@"Helvetica" size:14];
    //email.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
    email.delegate=self;
    [self.view addSubview:email];
    
    UIView *sep2=[[UIView alloc]initWithFrame:CGRectMake(20, 175, self.view.frame.size.width-40, 1)];
    sep2.backgroundColor=[UIColor lightGrayColor];
    [self.view addSubview:sep2];
    
    
    addressLbl=[[UILabel alloc]initWithFrame:CGRectMake(20, 185, self.view.frame.size.width-40, 30)];
    addressLbl.text=@"Primary Shipping Address";
    addressLbl.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
    addressLbl.textColor=[UIColor blackColor];
    addressLbl.textAlignment=NSTextAlignmentLeft;
    [self.view addSubview:addressLbl];

    
    addressDetailLbl=[[UILabel alloc]initWithFrame:CGRectMake(20, 220, self.view.frame.size.width-70, 20)];
    addressDetailLbl.font = [UIFont fontWithName:@"Helvetica" size:14];
    addressDetailLbl.textColor=[UIColor blackColor];
    addressDetailLbl.numberOfLines=5.0;
    addressDetailLbl.textAlignment=NSTextAlignmentLeft;
    [self.view addSubview:addressDetailLbl];
    
    addressDetailLbl1=[[UILabel alloc]initWithFrame:CGRectMake(20, 240, self.view.frame.size.width-180, 20)];
    addressDetailLbl1.font = [UIFont fontWithName:@"Helvetica" size:14];
    addressDetailLbl1.textColor=[UIColor blackColor];
    addressDetailLbl1.numberOfLines=5.0;
    addressDetailLbl1.textAlignment=NSTextAlignmentLeft;
    [self.view addSubview:addressDetailLbl1];
    
    addressDetailLbl2=[[UILabel alloc]initWithFrame:CGRectMake(20, 260, self.view.frame.size.width-180, 20)];
    addressDetailLbl2.font = [UIFont fontWithName:@"Helvetica" size:14];
    addressDetailLbl2.textColor=[UIColor blackColor];
    addressDetailLbl2.numberOfLines=5.0;
    addressDetailLbl2.textAlignment=NSTextAlignmentLeft;
    [self.view addSubview:addressDetailLbl2];
    
    addressDetailLbl3=[[UILabel alloc]initWithFrame:CGRectMake(20, 280, self.view.frame.size.width-180, 20)];
    addressDetailLbl3.font = [UIFont fontWithName:@"Helvetica" size:14];
    addressDetailLbl3.textColor=[UIColor blackColor];
    addressDetailLbl3.numberOfLines=5.0;
    addressDetailLbl3.textAlignment=NSTextAlignmentLeft;
    [self.view addSubview:addressDetailLbl3];
    
    UIView *sep4=[[UIView alloc]initWithFrame:CGRectMake(20, 310, self.view.frame.size.width-40, 1)];
    sep4.backgroundColor=[UIColor lightGrayColor];
    [self.view addSubview:sep4];
    
    UIButton *editAddress = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [editAddress addTarget:self action:@selector(changeaddressAction:) forControlEvents:UIControlEventTouchUpInside];
    [editAddress setTitle:@"Change Address" forState:UIControlStateNormal];
    editAddress.frame = CGRectMake(20, 322, 160, 25);
    editAddress.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    editAddress.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
    [editAddress setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.view addSubview:editAddress];
    
    UIView *sep3=[[UIView alloc]initWithFrame:CGRectMake(20, 360, self.view.frame.size.width-40, 1)];
    sep3.backgroundColor=[UIColor lightGrayColor];
    [self.view addSubview:sep3];
    
    UIButton *changePass = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [changePass addTarget:self action:@selector(changePasswordAction:) forControlEvents:UIControlEventTouchUpInside];
    [changePass setTitle:@"Change Password" forState:UIControlStateNormal];
    changePass.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    changePass.frame = CGRectMake(20, 370, 160, 25);
    changePass.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];
    [changePass setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.view addSubview:changePass];
    
}
-(void)changeaddressAction:(UIButton*)btn
{
    changeAddressViewController  *smvc = [[changeAddressViewController alloc]init];
    [self.navigationController pushViewController:smvc animated:YES];
}
-(void)changePasswordAction:(UIButton*)btn
{
    changePasswordViewController *smvc = [[changePasswordViewController alloc]init];
    [self.navigationController pushViewController:smvc animated:YES];
}
-(void)back_action:(UIButton*)btn
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
 
    addressDetailLbl.text=@"No Address Found";
    username.text=@"";
    email.text=@"";
    
    NSString *userid = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userid"]];
    
    if(userid == (id)[NSNull null]|| userid.length == 0  || [userid isEqual:@"(null)"])
    {
        ViewController *controller = [[ViewController alloc]init];
        [self.navigationController pushViewController:controller animated:NO];
    }
    else
    {
        HUD=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.labelText=@"Please Wait";
        NSString *st=[NSString stringWithFormat:@"http://18.219.134.84/slim_api/public/users/show/%@", [[NSUserDefaults standardUserDefaults]valueForKey:@"userid"]];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager GET:st parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            NSLog(@"JSON: %@,    %@", responseObject,st);
            HUD.hidden=YES;
            NSDictionary *dict=[responseObject valueForKey:@"result"][0];
            
            if([[dict valueForKey:@"full_name"] isEqualToString:@""] && [[dict valueForKey:@"address_line1"] isEqualToString:@""] && [[dict valueForKey:@"address_line2"] isEqualToString:@""])
            {
                addressDetailLbl.text=@"No Address Found";
                username.text=[dict valueForKey:@"username"];
                email.text=[dict valueForKey:@"email"];
            }
            else
            {
                addressDetailLbl.text=[dict valueForKey:@"full_name"];
                
                addressDetailLbl1.text=[dict valueForKey:@"address_line1"];
                
                addressDetailLbl2.text=[dict valueForKey:@"address_line2"];
                
                addressDetailLbl3.text=[NSString stringWithFormat:@"%@,%@,%@",[dict valueForKey:@"city"],[dict valueForKey:@"state"],[dict valueForKey:@"pin_code"]];
                
                
                username.text=[dict valueForKey:@"username"];
                email.text=[dict valueForKey:@"email"];
                
            }
            
            
            
            
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];

    }
    if([[[NSUserDefaults standardUserDefaults]valueForKey:@"product"]isEqualToString:@"yes"])
    {
        self.tabBarController.selectedIndex=0;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)updateAction
{
  
}
- (void)menu_action:(UIButton*)btn
{
    [self addMenuView];
}
-(void)addMenuView
{
    menuV=[[UIView alloc]initWithFrame:CGRectMake(0, 24, self.view.frame.size.width, self.view.frame.size.height-24)];
    menuV.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    [[[[UIApplication sharedApplication] delegate] window] addSubview:menuV];
    
    UIView *v=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-60, self.view.frame.size.height-24)];
    v.backgroundColor=[UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1.0];
    [menuV addSubview:v];
    
    
    
    tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 120, v.frame.size.width, v.frame.size.height-120) style:UITableViewStylePlain];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [v addSubview:tableView];
    
    UILabel *skinTone = [[UILabel alloc] initWithFrame:CGRectMake (20, 30,v.frame.size.width-40, 70)];
    [skinTone setFont:[UIFont fontWithName:@"Helvetica" size:18]];
    skinTone.textColor=[UIColor colorWithRed:133/255.0 green:133/255.0 blue:133/255.0 alpha:1.0];
    skinTone.lineBreakMode=NSLineBreakByWordWrapping;
    skinTone.numberOfLines=6;
    skinTone.textAlignment=NSTextAlignmentCenter;
    skinTone.backgroundColor=[UIColor clearColor];
    
    if([[NSUserDefaults standardUserDefaults]valueForKey:@"username"])
    {
        [skinTone setText:[[NSUserDefaults standardUserDefaults]valueForKey:@"username"]];
    }
    else
        [skinTone setText:@""];
    
    [v addSubview:skinTone];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    backButton.frame = CGRectMake(menuV.frame.size.width-100, 10, 30, 30);
    [backButton setTitleColor:[UIColor colorWithRed:133/255.0 green:133/255.0 blue:133/255.0 alpha:1.0] forState:UIControlStateNormal];
    [backButton setTitle:@"" forState:UIControlStateNormal];
    UIImage *img = [[UIImage imageNamed:@"cross"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    [backButton setImage:img forState:UIControlStateNormal];
    backButton.layer.borderColor=[UIColor colorWithRed:133/255.0 green:133/255.0 blue:133/255.0 alpha:1.0].CGColor;
    backButton.layer.borderWidth=1.0;
    backButton.layer.cornerRadius=backButton.frame.size.width/2;
    backButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
    [backButton addTarget:self action:@selector(back_Actionn) forControlEvents:UIControlEventTouchUpInside];
    [menuV addSubview:backButton];
}
-(void)back_Actionn
{
    [menuV removeFromSuperview];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return menuArray.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CELL"];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    
    
    
    UIButton *changePass = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [changePass setTitle:[menuArray objectAtIndex:indexPath.row] forState:UIControlStateNormal];
    //    changePass.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    changePass.frame = CGRectMake(20, 10,tableView.frame.size.width-40, 20);
    changePass.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:13];
    UIImage *img2 = [[UIImage imageNamed:menuImageArray[indexPath.row]]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [changePass setImage:img2 forState:UIControlStateNormal];
    changePass.userInteractionEnabled=false;
    [changePass setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cell addSubview:changePass];
    
    
    //    UIFont * myFont =[UIFont fontWithName:@"Lato-Regular" size:12];
    //    CGRect labelFrame1 = CGRectMake (100, 5,tableView.frame.size.width-80, 30);
    //    UILabel *label1 = [[UILabel alloc] initWithFrame:labelFrame1];
    //    [label1 setFont:myFont];
    //    label1.textColor=[UIColor whiteColor];
    //    label1.textAlignment=NSTextAlignmentLeft;
    //    label1.backgroundColor=[UIColor clearColor];
    //    [label1 setText:[menuArray objectAtIndex:indexPath.row]];
    //    [cell addSubview:label1];
    //
    //    UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(77, 12, 16, 16)];
    //    img.image=[UIImage imageNamed:menuImageArray[indexPath.row]];
    //    [cell addSubview:img];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(50, 39, tableView.frame.size.width-100,0.3)];
    [view setBackgroundColor:[UIColor colorWithRed:255.0 green:255.0 blue:255.0 alpha:0.1]];
    [cell addSubview:view];
    
    cell.backgroundColor=[UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0)
    {
        self.tabBarController.selectedIndex=2;
        [self back_Actionn];
    }
    if(indexPath.row==7)
    {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:@"" forKey:@"userid"];
        [userDefaults setObject:@"" forKey:@"username"];
        [userDefaults setObject:nil forKey:@"color"];
        
        [userDefaults synchronize];
        self.tabBarController.selectedIndex=2;
        [self back_Actionn];
    }
    if(indexPath.row==4)
    {
        self.tabBarController.selectedIndex=0;
        [self back_Actionn];
    }
    if(indexPath.row==5)
    {
        aboutUsViewController *controller = [[aboutUsViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
        [self back_Actionn];
    }
    if(indexPath.row==6)
    {
        contactUsViewController *controller = [[contactUsViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
        [self back_Actionn];
    }
    //    if(indexPath.row==7)
    //    {
    //        videoViewController *controller = [[videoViewController alloc]init];
    //        [self.navigationController pushViewController:controller animated:YES];
    //        [self back_Action];
    //    }
    if(indexPath.row==2)
    {
        historyViewController *controller = [[historyViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
        [self back_Actionn];
    }
    if(indexPath.row==1)
    {
        myOrderViewController *controller = [[myOrderViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
        [self back_Actionn];
    }
    if(indexPath.row==3)
    {
        notificationsViewController *controller = [[notificationsViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
        [self back_Actionn];
    }
    
    
    
}
@end
