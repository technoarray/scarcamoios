//
//  historyViewController.m
//  ScarCamo
//
//  Created by mac on 27/02/18.
//  Copyright © 2018 RohitMahajan. All rights reserved.
//

#import "historyViewController.h"
#import "AFNetworking.h"
@interface historyViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
{
    UIScrollView *scrv;
    UICollectionView *FeatureCollectionView;
    NSMutableDictionary *historyDict;
    NSArray *keyArray;
    UIView *backV;
}
@end

@implementation historyViewController
-(void)backAction:(UIButton*)btn
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView *navView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:navView];
    
    
    UIImageView *logo=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-68.5, 32, 137, 23)];
    logo.image=[UIImage imageNamed:@"logo1"];
    logo.contentMode = UIViewContentModeScaleAspectFit;
    [navView addSubview:logo];
    
    UIView *v=[[UIView alloc]initWithFrame:CGRectMake(0, 63, self.view.frame.size.width, 1)];
    v.backgroundColor=[UIColor lightGrayColor];
    v.alpha=0.5;
    [navView addSubview:v];
    

    UIImage *img = [[UIImage imageNamed:@"back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    menuButton.frame = CGRectMake(10,25, 40, 40);
    [menuButton setImage:img forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back_action:) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:menuButton];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    UILabel *nameLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 80, self.view.frame.size.width, 30)];
    nameLbl.text=@"HISTORY";
    nameLbl.font = [UIFont fontWithName:@"Helvetica" size:18];
    nameLbl.textColor=[UIColor blackColor];
    nameLbl.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:nameLbl];
    
    
    historyDict = [[[NSUserDefaults standardUserDefaults]valueForKey:@"historyy"]mutableCopy];
    keyArray=[NSArray arrayWithArray:[historyDict allKeys]];
    
   
            if([[historyDict allKeys] count]>0)
            {
            UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
            [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
            FeatureCollectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(10, 120, self.view.frame.size.width-20,self.view.frame.size.height-100) collectionViewLayout:layout];
            layout.minimumInteritemSpacing = 0;
            layout.minimumLineSpacing = 5;
            FeatureCollectionView.tag=90909090;
            [FeatureCollectionView setCollectionViewLayout:layout];
            [FeatureCollectionView setDataSource:self];
            [FeatureCollectionView setDelegate:self];
            [FeatureCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
            [FeatureCollectionView setBackgroundColor:[UIColor whiteColor]];
            [self.view addSubview:FeatureCollectionView];
            }
    else
    {
        UILabel *history=[[UILabel alloc]initWithFrame:CGRectMake(0, 130, self.view.frame.size.width, 30)];
        history.text=@"No History Found";
        history.font = [UIFont fontWithName:@"Helvetica" size:15];
        history.textColor=[UIColor blackColor];
        history.textAlignment=NSTextAlignmentCenter;
        [self.view addSubview:history];
    }
    

}
-(void)back_action:(UIButton*)btn
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
 
        return [[historyDict allKeys] count];
   
   
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    
    for(UIView *view in cell.subviews)
    {
        for(UIView *subView in view.subviews)
        {
            
            [subView removeFromSuperview];
        }
        [view removeFromSuperview];
    }
    
    cell.layer.borderColor=[UIColor lightGrayColor].CGColor;
    cell.layer.borderWidth=1.0;

    NSArray *arr=[[historyDict valueForKey:keyArray[indexPath.row]]mutableCopy];
         NSArray *items = [arr[0] componentsSeparatedByString:@","];
        UIView *backImage = [[UIView alloc]initWithFrame:CGRectMake(0, 0,cell.frame.size.width, cell.frame.size.height/2+cell.frame.size.height/5)];
        backImage.backgroundColor=[UIColor colorWithRed:[items[0]intValue]/255.0 green:[items[1]intValue]/255.0 blue:[items[2]intValue]/255.0 alpha:1.0];
        [cell addSubview:backImage];
        
        
    
    
            UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, cell.frame.size.height/2+cell.frame.size.height/5+1, FeatureCollectionView.frame.size.width/2-5, 20)];
            lbl.textColor=[UIColor blackColor];
            lbl.numberOfLines=2;
            lbl.textAlignment=NSTextAlignmentCenter;
            lbl.font=[UIFont fontWithName:@"Helvetica" size:16];
            lbl.text=[NSString stringWithFormat:@"%d",(int)indexPath.row+1];
            [cell addSubview:lbl];
    
           NSArray *foo=[keyArray[indexPath.row] componentsSeparatedByString:@" "];
            UILabel *lbl1 = [[UILabel alloc]initWithFrame:CGRectMake(0, cell.frame.size.height/2+cell.frame.size.height/5+20, FeatureCollectionView.frame.size.width/2-5, 20)];
            lbl1.textColor=[UIColor blackColor];
            lbl1.numberOfLines=2;
            lbl1.textAlignment=NSTextAlignmentCenter;
            lbl1.font=[UIFont fontWithName:@"Helvetica" size:16];
            lbl1.text=foo[0];
            [cell addSubview:lbl1];
            
    
  
    
    
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    
    cell.backgroundColor=[UIColor clearColor];
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
        return CGSizeMake(FeatureCollectionView.frame.size.width/2-5,FeatureCollectionView.frame.size.width/2);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    backV=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backV.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    [self.view addSubview:backV];
    
    
    UIView *labelView=[[UIView alloc]initWithFrame:CGRectMake(20, self.view.frame.size.height/2-150, self.view.frame.size.width-40, 300)];
    labelView.backgroundColor=[UIColor whiteColor];
    [backV addSubview:labelView];
    
    
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(labelView.frame.size.width/2-100, 15, 200, 100)];
    lbl.textColor=[UIColor blackColor];
    lbl.numberOfLines=3;
    lbl.textAlignment=NSTextAlignmentCenter;
    lbl.font=[UIFont fontWithName:@"Helvetica-Bold" size:18];
    lbl.text=[NSString stringWithFormat:@"%@",@"SKIN TONE CHANGES WITH CLIMATE AND SEASON"];
    [labelView addSubview:lbl];
    
    UILabel *lbl1 = [[UILabel alloc]initWithFrame:CGRectMake(labelView.frame.size.width/2-100, 120, 200, 50)];
    lbl1.textColor=[UIColor blackColor];
    lbl1.numberOfLines=2;
    lbl1.textAlignment=NSTextAlignmentCenter;
    lbl1.font=[UIFont fontWithName:@"Helvetica" size:14];
    lbl1.text=@"Do you still want to go with previous selection?";
    [labelView addSubview:lbl1];
    
    UIButton *yesBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    yesBtn.frame = CGRectMake(labelView.frame.size.width/2-40,200, 80, 40);
    [yesBtn setTitle:@"YES" forState:UIControlStateNormal];
    yesBtn.titleLabel.font=[UIFont fontWithName:@"Helvetica-Bold" size:15];
    yesBtn.tag=indexPath.row;
    [yesBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [yesBtn setBackgroundImage:[UIImage imageNamed:@"btn"] forState:UIControlStateNormal];
    [yesBtn addTarget:self action:@selector(yes_action:) forControlEvents:UIControlEventTouchUpInside];
    [labelView addSubview:yesBtn];
    
    UIButton *noButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    noButton.frame = CGRectMake(labelView.frame.size.width/2-40,250, 80, 40);
    noButton.tag=indexPath.row;
    noButton.titleLabel.font=[UIFont fontWithName:@"Helvetica-Bold" size:15];
    [noButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [noButton setBackgroundImage:[UIImage imageNamed:@"btn"] forState:UIControlStateNormal];
    [noButton setTitle:@"NO" forState:UIControlStateNormal];
    [noButton addTarget:self action:@selector(no_action:) forControlEvents:UIControlEventTouchUpInside];
    [labelView addSubview:noButton];
    
    
    
}

-(void)yes_action:(UIButton*)btn
{
    NSArray *arr=[[historyDict valueForKey:keyArray[btn.tag]]mutableCopy];
    
    
    
    NSArray *items = [[NSString stringWithFormat:@"%@",arr[0]] componentsSeparatedByString:@","];
    UIColor *color=[UIColor colorWithRed:[items[0]intValue]/255.0 green:[items[1]intValue]/255.0 blue:[items[2]intValue]/255.0 alpha:1.0];
    NSData *data1 = [NSKeyedArchiver archivedDataWithRootObject:color];
    [[NSUserDefaults standardUserDefaults]setValue:data1 forKey:@"color"];


    [[NSUserDefaults standardUserDefaults]setValue:arr[1] forKey:@"index"];
    [[NSUserDefaults standardUserDefaults]setValue:arr[0] forKey:@"color_code"];
    [[NSUserDefaults standardUserDefaults]synchronize];

    self.tabBarController.selectedIndex=0;
    [self.navigationController popToRootViewControllerAnimated:YES];
    

}

-(void)no_action:(UIButton*)btn
{
    [self.tabBarController setSelectedIndex:1];
    [backV removeFromSuperview];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
