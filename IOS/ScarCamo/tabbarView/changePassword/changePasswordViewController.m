//
//  changePasswordViewController.m
//  ScarCamo
//
//  Created by mac on 28/03/18.
//  Copyright © 2018 RohitMahajan. All rights reserved.
//

#import "changePasswordViewController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"

@interface changePasswordViewController ()<UITextFieldDelegate>
{
    UIButton *saveButton;
    UIScrollView *scrv;
    
    UITextField *fullName;
    UITextField *mobileNo;
    UITextField *ad1;
    UITextField *ad2;
    UITextField *city;
    UITextField *pincode;
    UITextField *state;
    MBProgressHUD *HUD;

}
@end

@implementation changePasswordViewController
-(void)backAction:(UIButton*)btn
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor=[UIColor whiteColor];
    
    UIView *navView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:navView];
    
    
    UIImageView *logo=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-68.5, 32, 137, 23)];
    logo.image=[UIImage imageNamed:@"logo1"];
    logo.contentMode = UIViewContentModeScaleAspectFit;
    [navView addSubview:logo];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    UIImage *img = [[UIImage imageNamed:@"back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    [backButton addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(10, 20, 44, 44);
    [backButton setImage:img forState:UIControlStateNormal];
    [navView addSubview:backButton];
    
    UIView *v=[[UIView alloc]initWithFrame:CGRectMake(0, 63, self.view.frame.size.width, 1)];
    v.backgroundColor=[UIColor lightGrayColor];
    v.alpha=0.5;
    [navView addSubview:v];
    
    
   
    
    scrv=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64-95)];
    
    [self.view addSubview:scrv];
    
    
    UILabel *nameLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 10, self.view.frame.size.width, 30)];
    nameLbl.text=@"CHANGE PASSWORD";
    nameLbl.font = [UIFont fontWithName:@"Helvetica" size:18];
    nameLbl.textColor=[UIColor blackColor];
    nameLbl.textAlignment=NSTextAlignmentCenter;
    [scrv addSubview:nameLbl];
    
    
    
    int y= 60;
    fullName= [[UITextField alloc]initWithFrame:CGRectMake(20, y, self.view.frame.size.width-40, 40)];
    fullName.placeholder=@"Old Password";
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    fullName.leftView = paddingView;
    fullName.secureTextEntry=true;
    fullName.leftViewMode = UITextFieldViewModeAlways;
    fullName.layer.borderColor=[UIColor lightGrayColor].CGColor;
    fullName.layer.borderWidth=1.0;
    fullName.delegate=self;
    [scrv addSubview:fullName];
    
    y=y+50;
    
    mobileNo= [[UITextField alloc]initWithFrame:CGRectMake(20,y , self.view.frame.size.width-40, 40)];
    mobileNo.placeholder=@"New Password";
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    mobileNo.leftView = paddingView1;
    mobileNo.secureTextEntry=true;
    mobileNo.leftViewMode = UITextFieldViewModeAlways;
    mobileNo.layer.borderColor=[UIColor lightGrayColor].CGColor;
    mobileNo.layer.borderWidth=1.0;
    mobileNo.delegate=self;
    [scrv addSubview:mobileNo];
    
    y=y+50;
    
    ad1= [[UITextField alloc]initWithFrame:CGRectMake(20,y , self.view.frame.size.width-40, 40)];
    ad1.placeholder=@"Conform New Password";
    ad1.secureTextEntry=true;

    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 40)];
    ad1.leftView = paddingView2;
    ad1.leftViewMode = UITextFieldViewModeAlways;
    ad1.layer.borderColor=[UIColor lightGrayColor].CGColor;
    ad1.layer.borderWidth=1.0;
    ad1.delegate=self;
    [scrv addSubview:ad1];
    
    y=y+50;
    
    saveButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [saveButton addTarget:self action:@selector(stripeCall) forControlEvents:UIControlEventTouchUpInside];
    [saveButton setTitle:@"Save" forState:UIControlStateNormal];
    saveButton.frame = CGRectMake(self.view.frame.size.width/2-100, y, 200, 50);
    saveButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:18];    fullName.layer.cornerRadius=5.0;
    [saveButton setBackgroundImage:[UIImage imageNamed:@"btn"] forState:UIControlStateNormal];
    [saveButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [scrv addSubview:saveButton];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    //    [scrv setContentOffset:CGPointMake(0, 0) animated:YES];
    [textField resignFirstResponder];
    return YES;
}
-(void)stripeCall
{
    if([mobileNo.text isEqualToString:ad1.text])
    {
        
    
    NSDictionary *params = @{@"uid"                   : [[NSUserDefaults standardUserDefaults]valueForKey:@"userid"],
                             @"oldpassword"           :fullName.text,
                             @"newpassword"           : mobileNo.text,
                             };
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        HUD=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.labelText=@"Please Wait";
    [manager POST:@"http://18.219.134.84/slim_api/public/users/changePassword" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"success!%@",[[responseObject valueForKey:@"result"][0]valueForKey:@"id"]);
        HUD.hidden=TRUE;
        if([[responseObject valueForKey:@"code"]intValue]==1)
        {
           
            [self.navigationController popViewControllerAnimated:YES];
            
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                                message:[responseObject valueForKey:@"message"]
                                                               delegate:nil
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil];
            [alertView show];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
        HUD.hidden=TRUE;

    }];
    
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                            message:@"New Password & Conform Password does not match"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
}


@end
