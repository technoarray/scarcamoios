//
//  aboutUsViewController.m
//  ScarCamo
//
//  Created by mac on 27/02/18.
//  Copyright © 2018 RohitMahajan. All rights reserved.
//

#import "aboutUsViewController.h"
#import "AsyncImageView.h"
#import "AFNetworking.h"

@interface aboutUsViewController ()

@end

@implementation aboutUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor whiteColor];
    UIView *navView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 64)];
    navView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:navView];
    
    
    UIImageView *logo=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-68.5, 32, 137, 23)];
    logo.image=[UIImage imageNamed:@"logo1"];
    logo.contentMode = UIViewContentModeScaleAspectFit;
    [navView addSubview:logo];
    
    UIView *v=[[UIView alloc]initWithFrame:CGRectMake(0, 63, self.view.frame.size.width, 1)];
    v.backgroundColor=[UIColor lightGrayColor];
    v.alpha=0.5;
    [navView addSubview:v];
    
    
    UIImage *img = [[UIImage imageNamed:@"back"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    menuButton.frame = CGRectMake(10,25, 40, 40);
    [menuButton setImage:img forState:UIControlStateNormal];
    [menuButton addTarget:self action:@selector(back_action:) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:menuButton];
    
    
    AsyncImageView *productImage=[[AsyncImageView alloc]initWithFrame:CGRectMake(0, 65, self.view.frame.size.width, self.view.frame.size.height/2)];
    productImage.backgroundColor=[UIColor clearColor];
    productImage.image =[UIImage imageNamed:@"about-us"];
    [self.view addSubview:productImage];
    
    
    UIView *downView = [[UIView alloc]initWithFrame:CGRectMake(20, self.view.frame.size.height/2-50, self.view.frame.size.width-40, self.view.frame.size.height/2-10)];
    downView.backgroundColor=[UIColor colorWithRed:255.0 green:255.0 blue:255.0 alpha:0.1];
    downView.layer.borderColor=[UIColor lightGrayColor].CGColor;
    downView.layer.borderWidth=0.5;
    [self.view addSubview:downView];
    
    
    UILabel *nameLbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 8, downView.frame.size.width, 30)];
    nameLbl.text=@"ABOUT US";
    nameLbl.backgroundColor=[UIColor clearColor];
    nameLbl.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    nameLbl.textColor=[UIColor blackColor];
    nameLbl.textAlignment=NSTextAlignmentCenter;
    [downView addSubview:nameLbl];
    
    UIView *v1=[[UIView alloc]initWithFrame:CGRectMake(20, 44, downView.frame.size.width-40, 1)];
    v1.backgroundColor=[UIColor lightGrayColor];
    v1.alpha=0.5;
    [downView addSubview:v1];
    
    UILabel *priceLbl=[[UILabel alloc]initWithFrame:CGRectMake(10, 50, downView.frame.size.width-20, self.view.frame.size.height/2-90)];
   // priceLbl.text=@"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages";
    priceLbl.backgroundColor=[UIColor clearColor];

    priceLbl.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
    if(self.view.frame.size.width>320)
        priceLbl.font = [UIFont fontWithName:@"Helvetica-Bold" size:15];

    priceLbl.textColor=[UIColor blackColor];
    priceLbl.numberOfLines=15;
    priceLbl.textAlignment=NSTextAlignmentCenter;
    [downView addSubview:priceLbl];
    
    
    NSString *st=[NSString stringWithFormat:@"http://18.219.134.84/slim_api/public/pages/aboutus"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:st parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
       // NSLog(@"JSON: %@,    %@", responseObject,st);
        
        NSDictionary *dict=[responseObject valueForKey:@"result"];

        priceLbl.text=[dict valueForKey:@"description"];
        
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    
}
-(void)back_action:(UIButton*)btn
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
