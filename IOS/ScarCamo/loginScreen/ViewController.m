//
//  ViewController.m
//  ScarCamo
//
//  Created by mac on 02/02/18.
//  Copyright © 2018 RohitMahajan. All rights reserved.
//

#import "ViewController.h"
#import "registerViewController.h"
#import "AFNetworking.h"
#import "introViewController.h"
#import "MBProgressHUD.h"

@interface ViewController ()<UITextFieldDelegate>
{
    NSString *emailid123;
    MBProgressHUD *HUD;
 UIScrollView *scrv;
}
@end

@implementation ViewController
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden=true;
    
    UIImageView *bg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    bg.image=[UIImage imageNamed:@"login-bg"];
    [self.view addSubview:bg];
    
    
    scrv=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    scrv.contentSize=CGSizeMake(0, self.view.frame.size.height+110);
    [self.view addSubview:scrv];
    
    UIImageView *logo=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-95, 70, 190, 30)];
    logo.image=[UIImage imageNamed:@"logo"];
    logo.contentMode = UIViewContentModeScaleAspectFit;
    [scrv addSubview:logo];
    
    username=[[UITextField alloc]initWithFrame:CGRectMake(20, self.view.frame.size.height/2-50, self.view.frame.size.width-40, 40)];
    username.font = [UIFont fontWithName:@"Helvetica" size:15];
    username.textAlignment=NSTextAlignmentCenter;
    username.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    username.layer.borderColor=[UIColor whiteColor].CGColor;
    username.layer.borderWidth=1.0;
    username.textColor=[UIColor whiteColor];
    username.delegate=self;
    [scrv addSubview:username];
    
    
    UILabel *loginLbl=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-100, username.frame.origin.y-80, 200, 40)];
    loginLbl.text=@"LOG IN";
    loginLbl.font = [UIFont fontWithName:@"Helvetica" size:22];
    loginLbl.textColor=[UIColor whiteColor];
    loginLbl.textAlignment=NSTextAlignmentCenter;
    [scrv addSubview:loginLbl];
    
    
    
    
    password=[[UITextField alloc]initWithFrame:CGRectMake(20, self.view.frame.size.height/2, self.view.frame.size.width-40, 40)];
    password.textAlignment=NSTextAlignmentCenter;
    password.font = [UIFont fontWithName:@"Helvetica" size:15];
    password.textColor=[UIColor whiteColor];
    password.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    password.layer.borderColor=[UIColor whiteColor].CGColor;
    password.layer.borderWidth=1.0;
    password.delegate=self;
    password.secureTextEntry=true;
    [scrv addSubview:password];
    
    UIButton *forgotButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [forgotButton addTarget:self action:@selector(forgotAction:) forControlEvents:UIControlEventTouchUpInside];
    [forgotButton setTitle:@"FORGOT PASSWORD" forState:UIControlStateNormal];
    forgotButton.frame = CGRectMake(self.view.frame.size.width-150, password.frame.origin.y+password.frame.size.height+5, 150, 25);
    forgotButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:12];
    [forgotButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [scrv addSubview:forgotButton];
    
    
    UIButton *loginButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [loginButton addTarget:self action:@selector(loginAction:) forControlEvents:UIControlEventTouchUpInside];
    [loginButton setTitle:@"LOG IN" forState:UIControlStateNormal];
    loginButton.frame = CGRectMake(self.view.frame.size.width/2-75, password.frame.origin.y+password.frame.size.height+50, 150, 50);
    loginButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
    [loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    loginButton.layer.borderWidth=1.0;
    loginButton.layer.borderColor=[UIColor whiteColor].CGColor;
    [scrv addSubview:loginButton];
    
    UIImageView *arrow_img=[[UIImageView alloc]initWithFrame:CGRectMake(loginButton.frame.size.width-30, loginButton.frame.size.height/2-10, 20, 20)];
    arrow_img.image=[UIImage imageNamed:@"arrow"];
    [loginButton addSubview:arrow_img];
    
    
    
    UIButton *signUp = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [signUp addTarget:self action:@selector(signUpAction:) forControlEvents:UIControlEventTouchUpInside];
    [signUp setTitle:@"Create a new account" forState:UIControlStateNormal];
    signUp.frame = CGRectMake(self.view.frame.size.width/2-100, self.view.frame.size.height-100, 200, 20);
    signUp.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
    [signUp setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [scrv addSubview:signUp];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    //    [scrv setContentOffset:CGPointMake(0, 0) animated:YES];
    [textField resignFirstResponder];
    return YES;
}
-(void)loginAction:(UIButton*)btn
{
    if([username.text isEqualToString:@""] || [password.text isEqualToString:@""])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                            message:@"Please enter username & password"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
    else
    {
    HUD=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText=@"Please Wait";
    NSUserDefaults *u = [NSUserDefaults standardUserDefaults];
    NSString *emaillll=[u objectForKey:@"deviceToken"];

    NSDictionary *params = @{@"username"       : username.text,
                             @"password"       : password.text,
                             @"deviceType"     :@"1",
                             @"deviceToken"    :emaillll
                             };
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [manager POST:@"http://18.219.134.84/slim_api/public/users/login" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        HUD.hidden=true;
        if([[responseObject valueForKey:@"code"]intValue]==1)
        {
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:[[responseObject valueForKey:@"data"]valueForKey:@"id"] forKey:@"userid"];
            [userDefaults setObject:[[responseObject valueForKey:@"data"]valueForKey:@"username"] forKey:@"username"];

            [userDefaults synchronize];
            [self.navigationController popViewControllerAnimated:YES];
//            introViewController *smvc = [[introViewController alloc]init];
//            [self.navigationController pushViewController:smvc animated:YES];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                                message:[responseObject valueForKey:@"message"]
                                                               delegate:nil
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil];
            [alertView show];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error: %@", error);
    }];
    
    }
    
    
    

    

}
-(void)signUpAction:(UIButton*)btn
{
    registerViewController *smvc = [[registerViewController alloc]init];
    [self.navigationController pushViewController:smvc animated:YES];
}
-(IBAction)forgotAction:(id)sender
{
    UIAlertView* alert1 = [[UIAlertView alloc] init];
    [alert1 setDelegate:self];
    [alert1 setTitle:@"Enter Valid Email Id"];
    [alert1 setMessage:@" "];
    [alert1 addButtonWithTitle:@"Proceed"];
    [alert1 addButtonWithTitle:@"Cancel"];
    alert1.tag=1;
    alert1.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert1 show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag==1)
        
    {
        if(buttonIndex==0)
            
        {
            if([[alertView textFieldAtIndex:0].text isEqual:@""])
            {
                [self forgotAction:0];
            }
            else
            {
                emailid123 = [alertView textFieldAtIndex:0].text;
                NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
                NSPredicate *emailTest =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
                BOOL myStringMatchesRegEx=[emailTest evaluateWithObject:emailid123];
                if(!myStringMatchesRegEx)
                {
                    UIAlertView *al=[[UIAlertView alloc]initWithTitle:@"" message:@" Please enter valid email id " delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    al.tag=2;
                    [al show];
                }
                else
                {
                    HUD=[MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    HUD.labelText=@"Please Wait";
                    
                    
                    
                    
            
                    
                    
                   
                    NSDictionary *params=@{
                                           @"email"    : emailid123,
                                           };
                    
                    
                    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
                    manager.requestSerializer = [AFJSONRequestSerializer serializer];
                    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
                    
                    [manager POST:@"http://18.219.134.84/slim_api/public/users/forgetPassword" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                        NSLog(@"success!");
                        HUD.hidden=true;
                        if([[responseObject valueForKey:@"code"]intValue]==1)
                        {
                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                                                message:[responseObject valueForKey:@"message"]
                                                                               delegate:nil
                                                                      cancelButtonTitle:@"Ok"
                                                                      otherButtonTitles:nil];
                            [alertView show];

                        }
                        else
                        {
                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                                                message:[responseObject valueForKey:@"message"]
                                                                               delegate:nil
                                                                      cancelButtonTitle:@"Ok"
                                                                      otherButtonTitles:nil];
                            [alertView show];
                        }
                    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                        NSLog(@"error: %@", error);
                    }];
                    
                    
                    
        
    
    
}
            }
        }
    }
}

@end
